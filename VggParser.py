import json

x = 0
y = 0
channels = 0
input_layers_names = []

class Layer:
    def __init__(self, output_layers, node, config):
        self.name = node["name"]
        self.className = node["class_name"]
        self.config = config
        try:
            self.input = [i[0] for i in node["inbound_nodes"][0]]
            for i in self.input:
                if i in input_layers_names:
                    self.isInputLayer = True
                    self.input = []
                    break
                else:
                    self.isInputLayer = False
        except:
            self.input = []
            self.isInputLayer = True

        try:
            if self.name in output_layers:
                self.isOutputLayer = True
            else:
                self.isOutputLayer = False
        except:
            self.isOutputLayer = False
            
        self.inputDataPointerName = ""
        self.inputSizeInStr = ""
        self.outputDataPointerName = ""
        self.outputSizeInStr = ""

class LayerConfig:
    def __init__(self, config):
        self.name = config["name"]
        try:
            self.x = x
        except:
            self.x = 1
        try:
            self.y = y
        except:
            self.y = 1
        try:
            self.depth = config["filters"]
        except:
            self.depth = 1
        try:
            self.stride = config["strides"][0]
        except:
            self.stride = 1
        try:
            self.inputOffset = config["inputOffset"]
        except:
            self.inputOffset = 0
        try:
            self.outputOffset = config["outputOffset"]
        except:
            self.outputOffset = 0

    def __getitem__(self, item):
        return getattr(self, item)


class Relu(LayerConfig):
    def __init__(self, config):
        super().__init__(config)
        self.x = x
        self.y = y
        self.size = self.x * self.y * self.depth

class Softmax(LayerConfig):
    def __init__(self, config):
        super().__init__(config)
        self.x = x
        self.y = y
        self.size = self.x * self.y * self.depth

class Convolution2D(LayerConfig):
    convolutionCounter = 1
    def __init__(self, config):
        super().__init__(config)
        self.trainable = config["trainable"]
        self.activation = config["activation"]
        self.dataFormat = config["data_format"]
        self.dilationRate = config["dilation_rate"]
        self.useBias = config["use_bias"]
        self.kernelRegularizer = config["kernel_regularizer"]
        self.biasRegularizer = config["bias_regularizer"]
        self.activityRegularizer = config["activity_regularizer"]
        self.kernelConstraint = config["kernel_constraint"]
        self.biasConstaint = config["bias_constraint"]
        # self.kernelInitializer = config["kernel_initializer"]
        # self.biasInitializer = config["bias_initializer"]

        self.channels = channels
        self.filterSize = config["kernel_size"][0]
        self.stride = config["strides"][0]
        if config["padding"] == "same":
            self.padding = self.stride
        else:
            self.padding = 0
        self.offset = 0
        self.coeffFile = "coeff" + str(Convolution2D.convolutionCounter) + ".bin"
        self.biasFile = "bias" + str(Convolution2D.convolutionCounter) + ".bin"
        self.biasPointerName = ""
        self.coeffPointerName = ""
        Convolution2D.convolutionCounter += 1

class MaxPooling2D(LayerConfig):
    def __init__(self, config):
        super().__init__(config)
        self.trainable = config["trainable"]
        self.poolSize = config["pool_size"]
        self.padding = config["padding"]
        self.strides = config["strides"]
        self.dataFormat = config["data_format"]

        self.depth = channels
        self.size = channels
        self.padding = config["padding"]
        self.stride = config["strides"][0]
        if config["padding"] == "same":
            self.padding = self.stride
        else:
            self.padding = 0

class Flatten(LayerConfig):
    def __init__(self, config):
        super().__init__(config)
        self.name = config["name"]
        self.trainable = config["trainable"]
        self.dataFormat = config["data_format"]

        self.depth = channels

class Dense(LayerConfig):
    def __init__(self, config):
        super().__init__(config)
        self.name = config["name"]
        self.trainable = config["trainable"]
        self.units = config["units"]
        self.activation = config["activation"]
        self.useBias = config["use_bias"]
        # self.kernelInitializer = config["kernel_initializer"]
        # self.biasInitializer = config["bias_initializer"]
        self.kernelRegularizer = config["kernel_regularizer"]
        self.biasRegularizer = config["bias_regularizer"]
        self.activityRegularizer = config["activity_regularizer"]
        self.kernelConstraint = config["kernel_constraint"]
        self.biasConstaint = config["bias_constraint"]

        self.depth = channels

class InputLayer(LayerConfig):
    def __init__(self, config):
        self.name = config["name"]
        self.inputShape = config["batch_input_shape"]
        self.inputDtype = config["dtype"]
        self.sparse = config["sparse"]

def readJson(jsonFile):
    global x
    global y
    global channels

    listOfNodes = []
    dictionary = open(jsonFile)
    general_config = json.load(dictionary)["config"]
    output_layers = [i[0] for i in general_config["output_layers"]]
    nodes = general_config["layers"]
    next_input_layer = ""

    for i in range(len(nodes)):
        node = nodes[i]
        config = node["config"]
        className = node["class_name"]

        if className == "Conv2D":
            layer_to_add = Layer(output_layers, node, Convolution2D(config))
            if next_input_layer != "":
                layer_to_add.input.clear()
                layer_to_add.input.append(next_input_layer)
            listOfNodes.append(layer_to_add)
            next_input_layer = ""
            if node["config"]["activation"] == "relu":
                activationLayer = Layer(output_layers, node, Relu(config))
                activationLayer.className = "Relu"
                activationLayer.name = layer_to_add.name + "_act"
                activationLayer.isInputLayer = False
                next_input_layer = activationLayer.name
                activationLayer.input.clear()
                activationLayer.input.append(layer_to_add.name)
                listOfNodes.append(activationLayer)

        elif className == "InputLayer":
            x = node["config"]["batch_input_shape"][1]
            y = node["config"]["batch_input_shape"][2]
            channels = node["config"]["batch_input_shape"][3]
            input_layers_names.append(node["name"])
            next_input_layer = ""

        elif className == "MaxPooling2D":
            layer_to_add = Layer(output_layers, node, MaxPooling2D(config))
            if next_input_layer != "":
                layer_to_add.input.clear()
                layer_to_add.input.append(next_input_layer)
            listOfNodes.append(layer_to_add)
            next_input_layer = ""

        elif className == "Dense":
            layer_to_add = Layer(output_layers, node, Dense(config))
            if next_input_layer != "":
                layer_to_add.input.clear()
                layer_to_add.input.append(next_input_layer)
            listOfNodes.append(layer_to_add)
            next_input_layer = ""
            if node["config"]["activation"] == "relu":
                activationLayer = Layer(output_layers, node, Relu(config))
                activationLayer.className = "Relu"
                activationLayer.name = layer_to_add.name + "_act"
                next_input_layer = activationLayer.name
                activationLayer.input.clear()
                activationLayer.input.append(layer_to_add.name)
                activationLayer.isInputLayer = False
                listOfNodes.append(activationLayer)
            if node["config"]["activation"] == "softmax":
                activationLayer = Layer(output_layers, node, Softmax(config))
                activationLayer.className = "Softmax"
                activationLayer.name = layer_to_add.name + "_act"
                next_input_layer = activationLayer.name
                activationLayer.isInputLayer = False
                activationLayer.input.clear()
                activationLayer.input.append(layer_to_add.name)
                listOfNodes.append(activationLayer)

        elif className == "Flatten":
            layer_to_add = Layer(output_layers, node, Flatten(config))
            if next_input_layer != "":
                layer_to_add.input.clear()
                layer_to_add.input.append(next_input_layer)
            listOfNodes.append(layer_to_add)
            next_input_layer = ""

    return listOfNodes

if __name__ == "__main__":
    file="vgg16.json"
    nodes = readJson(file)