from CodeGen import *
from AlexnetParser import *



def separator():
    return ", "


def functionEndline():
    return ");"


def functionStartLine():
    return "("


def LayerConf(y):
    layerconf = ILayerConfig(y)
    return layerconf.x


def featureMapDefinition():
    featureMap = []
    featureMap.append("float* feature_map = (float*)malloc(sizeof(float) * 227 * 227 * 3 * BATCH_SIZE);")
    featureMap.append("float* feature_map_1 = (float*)malloc(sizeof(float) * 55 * 55 * 96 * NUMBER_OF_THREADS);")
    featureMap.append("float* feature_map_2 = (float*)malloc(sizeof(float) * 55 * 55 * 96 * NUMBER_OF_THREADS);")
    featureMap.append("float* feature_map_3 = (float*)malloc(sizeof(float) * 27 * 27 * 96 * NUMBER_OF_THREADS);")
    featureMap.append("float* feature_map_4 = (float*)malloc(sizeof(float) * 27 * 27 * 256 * NUMBER_OF_THREADS);")
    featureMap.append("float* feature_map_5 = (float*)malloc(sizeof(float) * 27 * 27 * 256 * NUMBER_OF_THREADS);")
    featureMap.append("float* feature_map_6 = (float*)malloc(sizeof(float) * 27 * 27 * 256 * NUMBER_OF_THREADS);")
    featureMap.append("float* feature_map_7 = (float*)malloc(sizeof(float) * 13 * 13 * 384 * NUMBER_OF_THREADS);")
    featureMap.append("float* feature_map_8 = (float*)malloc(sizeof(float) * 13 * 13 * 384 * NUMBER_OF_THREADS);")
    featureMap.append("float* feature_map_9 = (float*)malloc(sizeof(float) * 13 * 13 * 384 * NUMBER_OF_THREADS);")
    featureMap.append("float* feature_map_10 = (float*)malloc(sizeof(float) * 13 * 13 * 384 * NUMBER_OF_THREADS);")
    featureMap.append("float* feature_map_11 = (float*)malloc(sizeof(float) * 13 * 13 * 256 * NUMBER_OF_THREADS);")
    featureMap.append("float* feature_map_12 = (float*)malloc(sizeof(float) * 13 * 13 * 256 * NUMBER_OF_THREADS);")
    featureMap.append("float* feature_map_13 = (float*)malloc(sizeof(float) * 6 * 6 * 256 * NUMBER_OF_THREADS);")
    featureMap.append("float* feature_map_14 = (float*)malloc(sizeof(float) * 1 * 1 * 4096 * NUMBER_OF_THREADS);")
    featureMap.append("float* feature_map_15 = (float*)malloc(sizeof(float) * 1 * 1 * 4096 * NUMBER_OF_THREADS);")
    featureMap.append("float* feature_map_16 = (float*)malloc(sizeof(float) * 1 * 1 * 4096 * NUMBER_OF_THREADS);")
    featureMap.append("float* feature_map_17 = (float*)malloc(sizeof(float) * 1 * 1 * 4096 * NUMBER_OF_THREADS);")
    featureMap.append("float* feature_map_18 = (float*)malloc(sizeof(float) * 1 * 1 * 1000 * NUMBER_OF_THREADS);")
    return featureMap

def coefDefinition():
    coefList =[]
    coefList.append("coeff_1 = (float *)malloc(11 * 11 * 3 * 96 * sizeof(float));");
    coefList.append("coeff_2 = (float *)malloc(5 * 5 * 48 * 256 * sizeof(float));");
    coefList.append("coeff_3 = (float *)malloc(3 * 3 * 256 * 384 * sizeof(float));");
    coefList.append("coeff_4 = (float *)malloc(3 * 3 * 192 * 384 * sizeof(float));");
    coefList.append("coeff_5 = (float *)malloc(3 * 3 * 192 * 256 * sizeof(float));");
    coefList.append("coeff_6 = (float *)malloc(6 * 6 * 256 * 4096 * sizeof(float));");
    coefList.append("coeff_7 = (float *)malloc(1 * 1 * 4096 * 4096 * sizeof(float));");
    coefList.append("coeff_8 = (float *)malloc(1 * 1 * 4096 * 1000 * sizeof(float));");

def GetCoefFile(convClass):
    result = ""

    if '1' in str(convClass.coeffFile):
        result += """loadCoeff(coeff_1, 11 * 11 * 3 * 96, "conv1.bin");"""
        convClass.coeffFile = "conv_1"
        return result
    if '2' in str(convClass.biasFile):
        result += """ loadCoeff(coeff_2, 5 * 5 * 48 * 256, "conv2.bin");"""
        convClass.coeffFile = "conv_2"
        return result
    if '3' in str(convClass.biasFile):
        result += """ loadCoeff(coeff_3, 3 * 3 * 256 * 384, "conv3.bin");"""
        convClass.coeffFile = "conv_3"
        return result
    if '4' in str(convClass.biasFile):
        result += """loadCoeff(coeff_4, 3 * 3 * 192 * 384, "conv4.bin");"""
        convClass.coeffFile = "conv_4"
        return result
    if '5' in str(convClass.biasFile):
        result += """loadCoeff(coeff_5, 3 * 3 * 192 * 256, "conv5.bin");"""
        convClass.coeffFile = "conv_5"
        return result
    if '6' in str(convClass.biasFile):
        result += """loadCoeff(coeff_6, 6 * 6 * 256 * 4096, "conv6.bin");"""
        convClass.coeffFile = "conv_6"
        return result
    if '7' in str(convClass.biasFile):
        result += """loadCoeff(coeff_7, 1 * 1 * 4096 * 4096, "conv7.bin");"""
        convClass.coeffFile = "conv_7"
        return result
    if '8' in str(convClass.biasFile):
        result += """loadCoeff(coeff_8, 1 * 1 * 4096 * 1000, "conv8.bin");"""
        convClass.coeffFile = "conv_8"
        return result

def biasDefinition():
    biasList = []
    biasList.append("bias_1 = (float *)malloc(96 * sizeof(float));")
    biasList.append("bias_2 = (float *)malloc(256 * sizeof(float));")
    biasList.append("bias_3 = (float *) malloc(384 * sizeof(float));")
    biasList.append("bias_4 = (float *)malloc(384 * sizeof(float));")
    biasList.append("bias_5 = (float *)malloc(256 * sizeof(float));")
    biasList.append("bias_6 = (float *)malloc(4096 * sizeof(float));")
    biasList.append("bias_7 = (float *)malloc(4096 * sizeof(float));")
    biasList.append("bias_8 = (float *)malloc(1000 * sizeof(float));")
    return biasList

def GetBiasFile(convClass):
    result = ""

    if '1' in str(convClass.biasFile):
        result += """loadCoeff(bias_1, 96, "bias1.bin");"""
        convClass.biasFile = "bias_1"
        return result
    if '2' in str(convClass.biasFile):
        result += """loadCoeff(bias_2, 256, "bias2.bin");"""
        convClass.biasFile = "bias_2"
        return result
    if '3' in str(convClass.biasFile):
        result += """loadCoeff(bias_3, 384, "bias3.bin");"""
        convClass.biasFile = "bias_3"
        return result
    if '4' in str(convClass.biasFile):
        result += """loadCoeff(bias_4, 384 "bias4.bin");"""
        convClass.biasFile = "bias_4"
        return result
    if '5' in str(convClass.biasFile):
        result += """loadCoeff(bias_5, 256, "bias5.bin");"""
        convClass.biasFile = "bias_5"
        return result
    if '6' in str(convClass.biasFile):
        result += """loadCoeff(bias_6, 4096, "bias6.bin");"""
        convClass.biasFile = "bias_6"
        return result
    if '7' in str(convClass.biasFile):
        result += """loadCoeff(bias_7, 4096, "bias7.bin");"""
        convClass.biasFile = "bias_7"
        return result
    if '8' in str(convClass.biasFile):
        result += """loadCoeff(bias_8, 1000, "bias8.bin");"""
        convClass.biasFile = "bias_8"
        return result

def GetFeatureMap(i):
    if i == 0:
        return "inputs_im"
    if i >= 1:
        return "feature_map_" + str(i)

        # conv_layer_1(int x, int y, float* feature_map, int channels, int depth, float *coeff, int filter_size,

def Conv(y,previousDepth,nextDepth):
    convClass = Convolution2D(y)

    result = GetBiasFile(convClass)
    cpp(result)
    result = GetCoefFile(convClass)
    cpp(result)
    result = ""
    result += "conv_layer1"
    result += functionStartLine()
    result += str(convClass.x) + separator()
    result += str(convClass.y) + separator()
    result += featureMap + "+ tid *" + str(convClass.x) + "*" + str(convClass.y) + "*" + previousDepth + separator()
    result += str(convClass.channels) + separator()
    result += str(convClass.depth)+separator()
    result += str(convClass.coeffFile) + separator()
    result += str(convClass.filterSize) + separator()
    result += str(convClass.stride) + separator()
    result += str(convClass.padding) + separator()
    result += outputFeatureMap + " + tid *" + str(convClass.x) + "*" + str(convClass.y) + "*" + str(nextDepth) + separator()
    result += str(convClass.offset) + separator()
    result += str(convClass.biasFile)
    result += functionEndline()
    return result

def GetDepth(className,config):
    if className == "Convolution2D":
        return GetConvDepth(config)
    elif className == "Relu":
        return GetReluDepth(config)
    elif className == "Pooling":
        return GetPoolDepth(config)

def GetConvDepth(y):
    return str(Convolution2D(y).depth)


def GetReluDepth(y):
    return str(Relu(y).depth)

def GetPoolDepth(y):
    return str(Pooling(y).depth)

def ReluDefinition(y,previousDepth,nextDepth):
    reluClass = Relu(y)
    result = "relu_layer("
    result += functionStartLine()
    result += str(reluClass.x) + separator()
    result += str(reluClass.y) + separator()
    result += featureMap + "+ tid *" + str(reluClass.x) + "*" + str(reluClass.y) + "*" + previousDepth + separator()
    result += str(reluClass.depth) + separator()
    result += str(reluClass.size) + separator()
    result += str(reluClass.stride) + separator()
    result += outputFeatureMap + " + " + "tid *" + str(reluClass.x) + "*" + str(reluClass.y) + "*" + nextDepth
    result += functionEndline()
    return result


def PoolingDefinition(y,previousDepth,nextDepth):
    poolingClass = Pooling(y)
    result = "pooling_layer"
    result += functionStartLine()
    result += str(poolingClass.x) + separator()
    result += str(poolingClass.y) + separator()
    result += featureMap + "+ tid *" + str(poolingClass.x) + "*" + str(poolingClass.y) + "*" + previousDepth + separator()
    result += str(poolingClass.depth) + separator()
    result += str(poolingClass.size) + separator()
    result += str(poolingClass.stride) + separator()
    result += str(poolingClass.padding) + separator()
    result += outputFeatureMap + " + " + "tid *" + str(poolingClass.x) + "*" + str(poolingClass.y) + "*" + nextDepth
    result += functionEndline()
    return result


def SoftmaxDefinition(y,previousDepth):
    softClass = Softmax(y)
    result = "softmax"
    result += functionStartLine()
    result += featureMap + "+ tid *" + previousDepth + separator()
    result += str(softClass.size) + separator()
    result += outputFeatureMap + " + " + "tid * 1000"
    result += functionEndline()
    return result


def getFunctionName(jsonFunctionName):
    return{
        'Convolution2D': 'conv_layer_1',
        'Relu': 'relu_layer',
        'Pooling': 'pooling_layer',
        'Softmax': 'softmax'
    }.get(jsonFunctionName, 'unknown')


filePathAlex = "alexnet.json"
filePathRes = "resnet50.json"
generatedClassNameAlex = "ModelAlex"
generatedClassNameRes = "ModelRes"
convCount = 0
nodes = readJson(filePathAlex)
cpp = CppFile(generatedClassNameAlex+".c")

with cpp.block("class " + generatedClassNameAlex):

    cpp("#define NUMBER_OF_THREADS 12")
    cpp("#define BATCH_SIZE 125")
    cpp("float *bias_1, *bias_2, *bias_3, *bias_4, *bias_5, *bias_6, *bias_7, *bias_8;")
    cpp("float *coeff_1, *coeff_2, *coeff_3, *coeff_4, *coeff_5, *coeff_6, *coeff_7, *coeff_8;")

    with cpp.block("void main()"):
        featureMapList = featureMapDefinition()
        biasList = biasDefinition()

        cpp("float* inputs_im = (float*)malloc(227 * 227 * 3 * BATCH_SIZE*sizeof(float));")
        cpp("int tid = omp_get_thread_num();")

        for j in range(len(featureMapDefinition())):
            cpp(featureMapList[j])

        for k in range(len(biasList)):
            cpp(biasList[k])

        configText = ""
        previousDepth = "1"
        nextDepth = "1"

        for i in range(len(nodes)):
            className = nodes[i].className
            config = nodes[i].config

            if i+1 < len(nodes):
                nextconf = nodes[i+1].config
                nextClassName = nodes[i+1].className
                if (nextClassName!="Softmax"):
                    nextDepth = GetDepth(nextClassName, nextconf)
                else:
                    nextDepth = "1"

            featureMap = GetFeatureMap(i)
            outputFeatureMap = GetFeatureMap(i+1)

            if className == "Convolution2D":
                configText += Conv(config,previousDepth,nextDepth)
            elif className == "Relu":
                configText += ReluDefinition(config,previousDepth,nextDepth)
            elif className == "Pooling":
                configText += PoolingDefinition(config,previousDepth,nextDepth)
            elif className == "Softmax":
                configText += SoftmaxDefinition(config,previousDepth)

            previousDepth = GetDepth(className,config)
            cpp(configText)
            configText=""

cpp.close()



