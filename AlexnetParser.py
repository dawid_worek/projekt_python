import json


class Layer:
    def __init__(self, node, config):
        self.name = node["name"]
        self.className = node["class_name"]
        self.config = config
        self.input = node["input"]
        try:
            self.isInputLayer = bool(node["isInputLayer"])
        except:
            self.isInputLayer = False
        try:
            self.isOutputLayer = bool(node["isOutputLayer"])
        except:
            self.isOutputLayer = False
            
        self.inputDataPointerName = ""
        self.inputSizeInStr = ""
        self.outputDataPointerName = ""
        self.outputSizeInStr = ""

class ILayerConfig:
    def __init__(self, config):
        try:
            self.x = config["x"]
        except:
            self.x = None
        try:
            self.y = config["y"]
        except:
            self.y = None
        try:
            self.depth = config["depth"]
        except:
            self.depth = None
        try:
            self.stride = config["stride"]
        except:
            self.stride = None
        try:
            self.inputOffset = config["inputOffset"]
        except:
            self.inputOffset = 0
        try:
            self.outputOffset = config["outputOffset"]
        except:
            self.outputOffset = 0

class Relu(ILayerConfig):
    def __init__(self, config):
        super().__init__(config)
        self.size = config["size"]
        self.x = config["x"]
        self.y = config["y"]
        self.depth = config["depth"]
        self.stride = config["stride"]

    def __getitem__(self, item):
        return getattr(self, item)


class Pooling(ILayerConfig):
    def __init__(self, config):
        super().__init__(config)
        self.size = config["size"]
        self.padding = config["padding"]
        self.x = config["x"]
        self.y = config["y"]
        self.depth = config["depth"]
        self.stride = config["stride"]

    def __getitem__(self, item):
        return getattr(self, item)


class Convolution2D(ILayerConfig):
    def __init__(self, config):
        super().__init__(config)
        self.channels = config["channels"]
        self.filterSize = config["filterSize"]
        self.padding = config["padding"]
        self.offset = config["offset"]
        self.coeffFile = config["coeffFile"]
        self.biasFile = config["biasFile"]
        self.x = config["x"]
        self.y = config["y"]
        self.depth = config["depth"]
        self.stride = config["stride"]
        self.biasPointerName = ""
        self.coeffPointerName = ""

    def __getitem__(self, item):
        return getattr(self, item)


class Softmax(ILayerConfig):
    def __init__(self, config):
        super().__init__(config)
        self.size = config["size"]

    def __getitem__(self, item):
        return getattr(self, item)


def readJson(jsonFile):
    listOfNodes = []
    dictionary = open(jsonFile)
    jsonFileParsed = json.load(dictionary)
    nodes = jsonFileParsed["config"]["layers"]
    for i in range(len(nodes)):
        node = nodes[i]
        config = node["config"]
        className = node["class_name"]
        if className == "Convolution2D":
            listOfNodes.append(Layer(node, Convolution2D(config)))
        elif className == "Relu":
            listOfNodes.append(Layer(node, Relu(config)))
        elif className == "Pooling":
            listOfNodes.append(Layer(node, Pooling(config)))
        elif className == "Softmax":
            listOfNodes.append(Layer(node, Softmax(config)))
        #print(str(i)+")", className) #printing name of class every object/layer node
    return jsonFileParsed["config"], listOfNodes



