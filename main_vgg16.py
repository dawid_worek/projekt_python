"""
To run this script the following packages are required:
 + Keras
 + Theano
 + h5py

Based on https://keras.io/applications/
"""

# Set Theano as backend for Keras
import os
os.environ['KERAS_BACKEND'] = 'theano'  # can be tensorflow or cntk

from keras.applications.vgg16 import VGG16
from keras.preprocessing import image
from keras.applications.vgg16 import preprocess_input
from common import saveModelToFiles
import numpy as np

if __name__ == '__main__':

    # Pre-trained model to test
    modelVGG16 = VGG16(include_top=False)

    img_path = 'elephant.jpg'
    img = image.load_img(img_path, target_size=(224, 224))
    x = image.img_to_array(img)
    x = np.expand_dims(x, axis=0)
    x = preprocess_input(x)
    
    # Save input
    inputBinFile = open("vgg16_input.bin", "wb")
    x.tofile(inputBinFile)
    inputBinFile.close()

    # Save model
    #saveModelToFiles(modelVGG16, 'vgg16')

    # Random input: dump input and output to files
    features = modelVGG16.predict(x)
    
    # Save output
    outputBinFile = open("vgg16_output.bin", "wb")
    features.tofile(outputBinFile)
    outputBinFile.close()