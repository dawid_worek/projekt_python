"""
Code Compiler - to speed up creating makefiles and compiling
@author Dawid Worek
@date 2018-11-09
"""
import subprocess
import os

class CodeCompiler:
    """
    This class helps to generate Makefile, compile it and run compiled program.
    """

    def __init__(self, buildDir = '_build', outputDir='.', cCompiler='gcc', cppCompiler = 'g++', makeCommand='make',
                        cStd = 'c11', cppStd = 'c++17', optimalization = '0', arCommand = 'ar'):
        self.buildDir = buildDir
        self.outputDir = outputDir
        self.cCompiler = cCompiler
        self.cppCompiler = cppCompiler
        self.makeCommand = makeCommand
        self.arCommand = arCommand
        self.cStd = cStd
        self.cppStd = cppStd
        self.optimalization = optimalization

        self.includeDirs = []
        self.cSources = []
        self.cppSources = []
        self.staticLibs = []
        self.cLinkerFlags = []
        self.cppLinkerFlags = []
        self.ccFlags = []
        self.cppFlags = []
        self.c_macros = []
        self.cpp_macros = []

    def add_source_to_compile(self, source):
        """
        Add source file to compile. Static libraries (*.a) are also supported here.
        @param source name or relative path to source file (*.c, *.cpp, *.cc). If static library is given here (*.a), it is also added
                    using @see add_static_library function
        """
        if source[-2:] == ".c":
            self.cSources.append(source)
        elif source[-4:] == ".cpp" or source[-3:] == ".cc":
            self.cppSources.append(source)
        elif source[-2:] == ".a":
            self.add_static_library(source)
        else:
            raise ValueError("Not supported extension (supported: *.c, *.cpp, *.cc)")

    def add_static_library(self, staticLib):
        """
        Use static library
        @param staticLib path to static library
        """
        self.staticLibs.append(staticLib)

    def add_include_dir(self, includeDir):
        """
        Add include dir (option -I)
        @param includeDir relative path to directory with headers
        """
        self.includeDirs.append(includeDir)

    def add_c_linker_flag(self, ldFlag):
        """
        Add linker flag (option -l)
        @param ldFlag flag to be added. Option '-l' is added automatically. For instance, to add pthread, just give 'pthread' as argument
        """
        self.cLinkerFlags.append(ldFlag)

    def add_cpp_linker_flag(self, ldFlag):
        """
        Add linker flag (option -l)
        @param ldFlag flag to be added. Option '-l' is added automatically. For instance, to add pthread, just give 'pthread' as argument
        """
        self.cppLinkerFlags.append(ldFlag)

    def add_c_flag(self, ccFlag):
        """
        Add option for C compiler with which project should be compiled. For instance, to become pedantic give '-pedantic', for PIC '-fPIC',
        to make warning errors '-Werror'
        @param ccFlag full name of the flag to add
        """
        self.ccFlags.append(ccFlag)

    def add_cpp_flag(self, cppFlag):
        """
        Add option for C++ compiler with which project should be compiled. For instance, to become pedantic give '-pedantic', for PIC '-fPIC',
        to make warning errors '-Werror'
        @param cxxFlag full name of the flag to add
        """
        self.cppFlags.append(cppFlag)

    def add_c_macro(self, macro):
        """
        Add macro with which C sources should be compiled.
        @param macro macro
        """
        self.c_macros.append(macro)

    def add_cpp_macro(self, macro):
        """
        Add macro with which C++ sources should be compiled.
        @param macro macro
        """
        self.cpp_macros.append(macro)

    def generate_makefile(self, projectDir = None, outputDir = '.', outputFileName = "program.out"):
        """
        Generate Makefile.
        @param projectDir path to main directory of the project. If not None, all files (including in subdirectories) with extensions:
                            *.c, *.cpp, *.cc will be added as sources, all subdirectories which contain files *.h or *.hpp will be 
                            added as includes
                            If None, only manually added includes and sources will be included in generated Makefile
        @param outputDir directory where Makefile will be generated
        @param outputFileName name of the output file. Can be relative path. If output file is *.a, static library is created
        """
        isStaticLibCompilation = False # True if output file should be static library
        if outputFileName[-2:] == ".a":
            isStaticLibCompilation = True

        # Add all *.c, *.cpp and *.cc to sources
        if projectDir != None:
            for dirpath, dirnames, filenames in os.walk(projectDir):
                for filename in [f for f in filenames if f.endswith(".c") or f.endswith(".cpp") or f.endswith(".cc") or f.endswith(".a")]:

                    pathInProjectDir = dirpath[(len(projectDir) + 1):]
                    self.add_source_to_compile( os.path.join(pathInProjectDir, filename) )

            # Add all directories with *.h and *.hpp files to include paths
            for dirpath, dirnames, filenames in os.walk(projectDir):
                for filename in [f for f in filenames if f.endswith(".h") or f.endswith(".hpp")]:
                    self.add_include_dir( dirpath[(len(projectDir) + 1):] )

        # Set flags to add
        self.ccFlags.append('--std=' + self.cStd)
        self.ccFlags.append('-O' + self.optimalization)
        self.cppFlags.append('--std=' + self.cppStd)
        self.cppFlags.append('-O' + self.optimalization)

        # Add macros
        self.ccFlags += ["-D" + macro for macro in self.c_macros]
        self.cppFlags += ["-D" + macro for macro in self.cpp_macros]

        # Open makefile to write to
        makefile = open(os.path.join(outputDir, 'Makefile'), 'w')
        
        # Set compilers
        makefile.write("CC=" + self.cCompiler + "\n")
        makefile.write("CXX=" + self.cppCompiler + "\n")

        # Set flags and include dictionaries
        includeFlags = ["-I" + incDir for incDir in self.includeDirs if incDir != '']
        makefile.write("CFLAGS=" + ' '.join(self.ccFlags + includeFlags) + "\n")
        makefile.write("CXXFLAGS=" + ' '.join(self.cppFlags + includeFlags) + "\n")

        # Set linker flags
        makefile.write("C_LDFLAGS=" + ' '.join(self.cLinkerFlags) + "\n")
        makefile.write("CXX_LDFLAGS=" + ' '.join(self.cppLinkerFlags) + "\n")

        # Add C sources
        if len(self.cSources) > 0:
            makefile.write("C_SRCS=" + ' '.join(self.cSources) + "\n")
            makefile.write("C_OBJS=$(C_SRCS:.c=.o)\n")
        else:
            makefile.write("C_OBJS= \n")

        # Add C++ sources
        if len(self.cppSources) > 0:
            makefile.write("CXX_SRCS=" + ' '.join(self.cppSources) + "\n")
            makefile.write("CXX_OBJS_TMP=$(CXX_SRCS:.cpp=.o)\n")
            makefile.write("CXX_OBJS=$(CXX_OBJS_TMP:.cc=.o)\n")
        else:
            makefile.write("CXX_OBJS= \n")

        # Add static libs
        if len(self.staticLibs) > 0:
            makefile.write("STATIC_LIBS= " +  " ".join(self.staticLibs) + "\n")
        else:
            makefile.write("STATIC_LIBS= \n")

        # Add build directory
        makefile.write("BUILD_DIR=" + self.buildDir + "\n")

        # Add directories inside build directory
        dirsToMake = []
        for s in (self.cSources + self.cppSources):
            pathToSource = os.path.split(s)
            if (len(pathToSource) >= 2) and pathToSource[0] != '.':
                pathToSource = pathToSource[:-1]
                for i in range(1, len(pathToSource) + 1):
                    dirsToMake.append( os.path.join(*pathToSource[:i]) )
        dirsToMake = list(set(dirsToMake)) # Remove duplications
        sorted(dirsToMake, key=lambda path: path.count('\\') + path.count('/')) # Sort by number of subdirectories
        dirsToMake = [os.path.join(self.buildDir, path) for path in dirsToMake if path != ""] # Add build directory to every path
        makefile.write("BUILD_DIRS=$(BUILD_DIR) " + ' '.join(dirsToMake) + "\n")

        # Add output file name
        makefile.write("OUTPUT=" + outputFileName + "\n")

        # Compile target for C files
        makefile.write("%.o: %.c\n\t$(CC) -c $< -o $(BUILD_DIR)/$@ $(CFLAGS) $(C_LDFLAGS)\n")

        # Compile target for C++ files
        makefile.write("%.o: %.cpp\n\t$(CXX) -c $< -o $(BUILD_DIR)/$@ $(CXXFLAGS) $(CXX_LDFLAGS)\n")
        makefile.write("%.o: %.cc\n\t$(CXX) -c $< -o $(BUILD_DIR)/$@ $(CXXFLAGS) $(CXX_LDFLAGS)\n")

        if isStaticLibCompilation:
            allSources = self.cSources + self.cppSources
            allBuildSources = []
            for s in allSources:
                if s[-2:] != ".a":
                    s = s.replace(".cpp", ".o")
                    s = s.replace(".c", ".o")
                    s = s.replace(".cc", ".o")
                    allBuildSources.append(os.path.join(self.buildDir, s))

            makefile.write("main:\n\t" + self.arCommand + " rsv $(OUTPUT) $(addprefix $(BUILD_DIR)/,$(C_OBJS)) $(addprefix $(BUILD_DIR)/,$(CXX_OBJS))\n")
        else:

            # Create target for main file
            if len(self.cppSources) > 0:
                makefile.write("main:\n\t$(CXX) $(addprefix $(BUILD_DIR)/,$(C_OBJS)) $(addprefix $(BUILD_DIR)/,$(CXX_OBJS)) $(STATIC_LIBS) -o $(OUTPUT) $(CXXFLAGS) $(CXX_LDFLAGS)\n")
            else:
                makefile.write("main:\n\t$(CC) $(addprefix $(BUILD_DIR)/,$(C_OBJS)) $(STATIC_LIBS) -o $(OUTPUT) $(CFLAGS) $(C_LDFLAGS)\n")

        # Create build directory
        makefile.write("createBuildDirs:\n\t-mkdir $(BUILD_DIRS)\n")

        # All target
        makefile.write("all: createBuildDirs $(C_OBJS) $(CXX_OBJS) main\n")

        # Clean target
        makefile.write("clean:\n\t-rm -r $(BUILD_DIR)\n")

        # PHONY target
        makefile.write(".PHONY: clean")

        # Close file - everything is written successfully
        makefile.close()

    def compile(self, outputFileName='output', projectDir = None):
        """
        Generate Makefile and compile code.
        @param outputFileName name of the output file. Can be relative path. If output file is *.a, static library is created
        @param projectDir path to main directory of the project. If None, only manually added includes and sources will be 
                            included in generated Makefile and Makefile will be generated in the current directory
                            (current directory is assumed to be main directoryu of the project)
        """
        if projectDir == None:
            self.generate_makefile(projectDir = projectDir, outputFileName=outputFileName)

            # Compile
            print("Running make...")
            try:
                print(subprocess.check_output([self.makeCommand, "all"]))
            except:
                print("Error while compiling. See logs above")
            else:
                print("Compilation done successfully")
        else:

            self.generate_makefile(projectDir = projectDir, outputDir = projectDir, outputFileName=outputFileName)

            # Compile
            print("Running make...")
            try:
                print(subprocess.check_output([self.makeCommand, "all"], cwd=os.path.join(os.getcwd(), projectDir)))
            except:
                print("Error while compiling. See logs above")
            else:
                print("Compilation done successfully")

    def compile_and_run(self, outputFileName='output', runCommand='./output', projectDir = None):
        """
        Generate Makefile, compile code and execute runCommand. It is executed in the same directory where output file is placed.
        @param outputFileName name of the output file. Can be relative path. If output file is *.a, static library is created
        @param runCommand command which is executed after successful compilation in the dictionary where output file is placed
        @param projectDir path to main directory of the project. If None, only manually added includes and sources will be 
                            included in generated Makefile (current directory is assumed to be main directoryu of the project)
        """
        compile(outputFileName=outputFileName, projectDir=projectDir)
        if projectDir == None:

            # Try to run program from the current directory
            try:
                print(subprocess.check_output([runCommand]))
            except:
                print("Error while running a program. See logs above")
        else:
            try:
                print(subprocess.check_output([runCommand], cwd=os.path.join(os.getcwd(), projectDir)))
            except:
                print("Error while running a program. See logs above")