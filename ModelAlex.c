class ModelAlex
{
	#define NUMBER_OF_THREADS 12
	#define BATCH_SIZE 125
	float *bias_1, *bias_2, *bias_3, *bias_4, *bias_5, *bias_6, *bias_7, *bias_8;
	float *coeff_1, *coeff_2, *coeff_3, *coeff_4, *coeff_5, *coeff_6, *coeff_7, *coeff_8;
	void main()
	{
		float* inputs_im = (float*)malloc(227 * 227 * 3 * BATCH_SIZE*sizeof(float));
		int tid = omp_get_thread_num();
		float* feature_map = (float*)malloc(sizeof(float) * 227 * 227 * 3 * BATCH_SIZE);
		float* feature_map_1 = (float*)malloc(sizeof(float) * 55 * 55 * 96 * NUMBER_OF_THREADS);
		float* feature_map_2 = (float*)malloc(sizeof(float) * 55 * 55 * 96 * NUMBER_OF_THREADS);
		float* feature_map_3 = (float*)malloc(sizeof(float) * 27 * 27 * 96 * NUMBER_OF_THREADS);
		float* feature_map_4 = (float*)malloc(sizeof(float) * 27 * 27 * 256 * NUMBER_OF_THREADS);
		float* feature_map_5 = (float*)malloc(sizeof(float) * 27 * 27 * 256 * NUMBER_OF_THREADS);
		float* feature_map_6 = (float*)malloc(sizeof(float) * 27 * 27 * 256 * NUMBER_OF_THREADS);
		float* feature_map_7 = (float*)malloc(sizeof(float) * 13 * 13 * 384 * NUMBER_OF_THREADS);
		float* feature_map_8 = (float*)malloc(sizeof(float) * 13 * 13 * 384 * NUMBER_OF_THREADS);
		float* feature_map_9 = (float*)malloc(sizeof(float) * 13 * 13 * 384 * NUMBER_OF_THREADS);
		float* feature_map_10 = (float*)malloc(sizeof(float) * 13 * 13 * 384 * NUMBER_OF_THREADS);
		float* feature_map_11 = (float*)malloc(sizeof(float) * 13 * 13 * 256 * NUMBER_OF_THREADS);
		float* feature_map_12 = (float*)malloc(sizeof(float) * 13 * 13 * 256 * NUMBER_OF_THREADS);
		float* feature_map_13 = (float*)malloc(sizeof(float) * 6 * 6 * 256 * NUMBER_OF_THREADS);
		float* feature_map_14 = (float*)malloc(sizeof(float) * 1 * 1 * 4096 * NUMBER_OF_THREADS);
		float* feature_map_15 = (float*)malloc(sizeof(float) * 1 * 1 * 4096 * NUMBER_OF_THREADS);
		float* feature_map_16 = (float*)malloc(sizeof(float) * 1 * 1 * 4096 * NUMBER_OF_THREADS);
		float* feature_map_17 = (float*)malloc(sizeof(float) * 1 * 1 * 4096 * NUMBER_OF_THREADS);
		float* feature_map_18 = (float*)malloc(sizeof(float) * 1 * 1 * 1000 * NUMBER_OF_THREADS);
		bias_1 = (float *)malloc(96 * sizeof(float));
		bias_2 = (float *)malloc(256 * sizeof(float));
		bias_3 = (float *) malloc(384 * sizeof(float));
		bias_4 = (float *)malloc(384 * sizeof(float));
		bias_5 = (float *)malloc(256 * sizeof(float));
		bias_6 = (float *)malloc(4096 * sizeof(float));
		bias_7 = (float *)malloc(4096 * sizeof(float));
		bias_8 = (float *)malloc(1000 * sizeof(float));
		loadCoeff(bias_1, 96, "bias1.bin");
		loadCoeff(coeff_1, 11 * 11 * 3 * 96, "conv1.bin");
		conv_layer1(227, 227, inputs_im+ tid *227*227*1, 3, 96, conv_1, 11, 4, 0, feature_map_1 + tid *227*227*96, 0, bias_1);
		relu_layer((55, 55, feature_map_1+ tid *55*55*96, 96, 290400, 0, feature_map_2 + tid *55*55*96);
		pooling_layer(55, 55, feature_map_2+ tid *55*55*96, 96, 3, 2, 0, feature_map_3 + tid *55*55*128);
		loadCoeff(bias_2, 256, "bias2.bin");
		 loadCoeff(coeff_2, 5 * 5 * 48 * 256, "conv2.bin");
		conv_layer1(27, 27, feature_map_3+ tid *27*27*96, 48, 128, conv_2, 5, 1, 2, feature_map_4 + tid *27*27*128, 0, bias_2);
		loadCoeff(bias_2, 256, "bias2.bin");
		 loadCoeff(coeff_2, 5 * 5 * 48 * 256, "conv2.bin");
		conv_layer1(27, 27, feature_map_4+ tid *27*27*128, 48, 128, conv_2, 5, 1, 2, feature_map_5 + tid *27*27*256, 128, bias_2);
		relu_layer((27, 27, feature_map_5+ tid *27*27*128, 256, 186624, 0, feature_map_6 + tid *27*27*256);
		pooling_layer(27, 27, feature_map_6+ tid *27*27*256, 256, 3, 2, 0, feature_map_7 + tid *27*27*384);
		loadCoeff(bias_4, 384 "bias4.bin");
		loadCoeff(coeff_4, 3 * 3 * 192 * 384, "conv4.bin");
		conv_layer1(13, 13, feature_map_7+ tid *13*13*256, 256, 384, conv_4, 3, 1, 1, feature_map_8 + tid *13*13*384, 0, bias_4);
		relu_layer((13, 13, feature_map_8+ tid *13*13*384, 384, 64896, 0, feature_map_9 + tid *13*13*128);
		loadCoeff(bias_5, 256, "bias5.bin");
		loadCoeff(coeff_5, 3 * 3 * 192 * 256, "conv5.bin");
		conv_layer1(13, 13, feature_map_9+ tid *13*13*384, 192, 128, conv_5, 3, 1, 1, feature_map_10 + tid *13*13*128, 0, bias_5);
		loadCoeff(bias_5, 256, "bias5.bin");
		loadCoeff(coeff_5, 3 * 3 * 192 * 256, "conv5.bin");
		conv_layer1(13, 13, feature_map_10+ tid *13*13*128, 192, 128, conv_5, 3, 1, 1, feature_map_11 + tid *13*13*256, 128, bias_5);
		relu_layer((13, 13, feature_map_11+ tid *13*13*128, 256, 43264, 0, feature_map_12 + tid *13*13*256);
		pooling_layer(13, 13, feature_map_12+ tid *13*13*256, 256, 3, 2, 0, feature_map_13 + tid *13*13*4096);
		loadCoeff(bias_6, 4096, "bias6.bin");
		loadCoeff(coeff_6, 6 * 6 * 256 * 4096, "conv6.bin");
		conv_layer1(6, 6, feature_map_13+ tid *6*6*256, 256, 4096, conv_6, 6, 1, 0, feature_map_14 + tid *6*6*4096, 0, bias_6);
		relu_layer((1, 1, feature_map_14+ tid *1*1*4096, 4096, 4096, 0, feature_map_15 + tid *1*1*4096);
		loadCoeff(bias_7, 4096, "bias7.bin");
		loadCoeff(coeff_7, 1 * 1 * 4096 * 4096, "conv7.bin");
		conv_layer1(1, 1, feature_map_15+ tid *1*1*4096, 4096, 4096, conv_7, 1, 1, 0, feature_map_16 + tid *1*1*4096, 0, bias_7);
		relu_layer((1, 1, feature_map_16+ tid *1*1*4096, 4096, 4096, 0, feature_map_17 + tid *1*1*1000);
		loadCoeff(bias_8, 1000, "bias8.bin");
		loadCoeff(coeff_8, 1 * 1 * 4096 * 1000, "conv8.bin");
		conv_layer1(1, 1, feature_map_17+ tid *1*1*4096, 4096, 1000, conv_8, 1, 1, 0, feature_map_18 + tid *1*1*1, 0, bias_8);
		softmax(feature_map_18+ tid *1000, 1000, feature_map_19 + tid * 1000);
	}
}
