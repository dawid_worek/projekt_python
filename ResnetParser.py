import json

class Layer:
    def __init__(self, node, config):
        self.name = node["name"]
        self.className = node["class_name"]
        self.config = config
        print(config)
        print(self.config)
        self.inboundNodes = node["inbound_nodes"]


class LayerConfig:
    name = ""


class Activation(LayerConfig):
    def __init__(self, config):
        self.name = config["name"]
        self.trainable = config["trainable"]
        self.activation = config["activation"]


class Convolution2D(LayerConfig):
    def __init__(self, config):
        self.name = config["name"]
        self.trainable = config["trainable"]
        self.activation = config["activation"]
        self.nbFilter = config["nb_filter"]
        self.nbRow = config["nb_row"]
        self.nbCol = config["nb_col"]
        self.init = config["init"]
        self.borderMode = config["border_mode"]
        self.subsample = config["subsample"]
        self.dimOrdering = config["dim_ordering"]
        self.wReg = config["W_regularizer"]
        self.bReg = config["b_regularizer"]
        self.activityReg = config["activity_regularizer"]
        self.wCon = config["W_constraint"]
        self.bCon = config["b_constraint"]
        self.bias = config["bias"]


class Merge(LayerConfig):
    def __init__(self, config):
        self.name = config["name"]
        self.mode = config["mode"]
        self.modeType = config["mode_type"]
        self.concatAxis = config["concat_axis"]
        self.dotAxes = config["dot_axes"]
        self.outputShape = config["output_shape"]
        self.outputShapeType = config["output_shape_type"]
        self.outputMask = config["output_mask"]
        self.outputMaskType = config["output_mask_type"]
        self.arguments = config["arguments"]


class BatchNormalization(LayerConfig):
    def __init__(self, config):
        self.name = config["name"]
        self.trainable = config["trainable"]
        self.epsilon = config["epsilon"]
        self.mode = config["mode"]
        self.gamma = config["gamma_regularizer"]
        self.beta = config["beta_regularizer"]
        self.momentum = config["momentum"]


class InputLayer(LayerConfig):
    def __init__(self, config):
        self.name = config["name"]
        self.inputShape = config["batch_input_shape"]
        self.inputDtype = config["input_dtype"]
        self.sparse = config["sparse"]


class MaxPooling2D(LayerConfig):
    def __init__(self, config):
        self.name = config["name"]
        self.trainable = config["trainable"]
        self.poolSize = config["pool_size"]
        self.borderMode = config["border_mode"]
        self.strides = config["strides"]
        self.dimOrdering = config["dim_ordering"]


class Flatten(LayerConfig):
    def __init__(self, config):
        self.name = config["name"]
        self.trainable = config["trainable"]


class Dense(LayerConfig):
    def __init__(self, config):
        self.name = config["name"]
        self.trainable = config["trainable"]
        self.outputDim = config["output_dim"]
        self.init = config["init"]
        self.wReg = config["W_regularizer"]
        self.bReg = config["b_regularizer"]
        self.activityReg = config["activity_regularizer"]
        self.wCon = config["W_constraint"]
        self.bCon = config["b_constraint"]
        self.bias = config["bias"]
        self.inputDim = config["input_dim"]


class ZeroPadding2D(LayerConfig):
    def __init__(self, config):
        self.name = config["name"]
        self.trainable = config["trainable"]
        self.padding = config["padding"]


class AveragePooling2D(LayerConfig):
    def __init__(self, config):
        self.name = config["name"]
        self.trainable = config["trainable"]
        self.poolSize = config["pool_size"]
        self.borderMode = config["border_mode"]
        self.strides = config["strides"]
        self.dimOrdering = config["dim_ordering"]


def readJson(jsonFile):
    listOfNodes = []
    dictionary = open(jsonFile)
    nodes = json.load(dictionary)["config"]["layers"]
    for i in range(len(nodes)):
        node = nodes[i]
        config = node["config"]
        className = node["class_name"]

        if className == "Convolution2D":
            listOfNodes.append(Layer(node, Convolution2D(config)))
        elif className == "Merge":
            listOfNodes.append(Layer(node, Merge(config)))
        elif className == "Activation":
            listOfNodes.append(Layer(node, Activation(config)))
        elif className == "BatchNormalization":
            listOfNodes.append(Layer(node, BatchNormalization(config)))
        elif className == "InputLayer":
            listOfNodes.append(Layer(node, InputLayer(config)))
        elif className == "MaxPooling2D":
            listOfNodes.append(Layer(node, MaxPooling2D(config)))
        elif className == "Dense":
            listOfNodes.append(Layer(node, Dense(config)))
        elif className == "Flatten":
            listOfNodes.append(Layer(node, Flatten(config)))
        elif className == "AveragePooling2D":
            listOfNodes.append(Layer(node, AveragePooling2D(config)))
        elif className == "ZeroPadding2D":
            listOfNodes.append(Layer(node, ZeroPadding2D(config)))

    return listOfNodes

file="resnet50.json"
nodes = readJson(file)