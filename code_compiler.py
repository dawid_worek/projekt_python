"""
Command-line front-end for CodeCompiler class. And, also, example of use of this class
@author Dawid Worek
@date 2018-11-09
"""

import sys
from CodeCompiler import CodeCompiler

def print_help():
    """
    Print help on standard output
    """
    print("code_compiler.py - tool to speed up creating makefiles and running compiled programs, v. 0.2")
    print("     -h, --help              print this help and exit")
    print("     -i <list_of_dirs>       manually add include directories")
    print("     -s <list_of_srcs>       manually add sources to compile. Statis libraries can also be here (*.a)")
    print("     -p <path_to_proj>       set path to main directory with project")
    print("     -c                      compile after successfully generating makefile")
    print("     --std <std_for_lang>    set standard of the language (default: c11 and c++17)")
    print("     -O <opt_level>          optimalization level (like in used compiler) (default: 0)")
    print("     -o <out_name>           name of output file (default: output). If output file is *.a, static library is created")
    print("     -f <list_of_options>    list options with which all sources should be compiled (without '-' at the beginning)")
    print("     -fc <list_of_options>   list options with which C sources should be compiled (without '-' at the beginning)")
    print("     -fcpp <list_of_options> list options with which C++ sources should be compiled (without '-' at the beginning)")
    print("     -lc <linker_flag>       add linker flag for C sources")
    print("     -lcpp <linker_flag>     add linker flag for C++ sources")
    print("     -D <list_of_macros>     add macros with which all sources should be compiled (-D option)")
    print("     -Dc <list_of_macros>    add macros with which only C sources should be compiled (-D option in gcc)")
    print("     -Dcpp <list_of_macros>  add macros with which only C++ sources should be compiled (-D option in g++)")
    print("     -ccomp <cmd>            command to run C compiler (default: gcc)")
    print("     -cppcomp <cmd>          command to run C++ compiler (default: g++)")
    print("Example: python code_compiler.py -p example_project -c --std c99 --std c++11 -f Wpedantic -fc Werror -O 2 -o example_project.exe")
    sys.exit(0)

if __name__ == "__main__":

    argvNumber = len(sys.argv)

    # Not enough arguments
    if argvNumber == 1:
        print_help()

    isToCompile = False
    projectDir = None
    outputFileName = "output"
    codeCompiler = CodeCompiler()

    for i in range(argvNumber):
        arg = sys.argv[i]

        if arg == "-h" or arg == "--help":
            print_help()

        elif arg == "-c":
            isToCompile = True

        elif arg == "-i":
            j = i + 1
            while (j < argvNumber) and not ('-' in sys.argv[j]):
                codeCompiler.add_include_dir(sys.argv[j])
                j += 1

        elif arg == "-s":
             j = i + 1
             while (j < argvNumber) and not ('-' in sys.argv[j]):
                codeCompiler.add_source_to_compile(sys.argv[j])
                j += 1

        elif arg == "-p":
            projectDir = sys.argv[i + 1]

        elif arg == "--std":
            standard = sys.argv[i + 1]
            if "+" in standard:
                codeCompiler.cppStd = standard
            else:
                codeCompiler.cStd = standard
        
        elif arg == "-O":
            codeCompiler.optimalization = sys.argv[i + 1]

        elif arg == "-o":
            outputFileName = sys.argv[i + 1]

        elif arg == "-f":
            j = i + 1
            while (j < argvNumber) and not ('-' in sys.argv[j]):
                codeCompiler.add_c_flag('-' + sys.argv[j])
                codeCompiler.add_cpp_flag('-' + sys.argv[j])
                j += 1

        elif arg == "-fc":
            j = i + 1
            while (j < argvNumber) and not ('-' in sys.argv[j]):
                codeCompiler.add_c_flag('-' + sys.argv[j])
                j += 1

        elif arg == "-fcpp":
            j = i + 1
            while (j < argvNumber) and not ('-' in sys.argv[j]):
                codeCompiler.add_cpp_flag('-' + sys.argv[j])
                j += 1

        elif arg == "-lc":
            j = i + 1
            while (j < argvNumber) and not ('-' in sys.argv[j]):
                codeCompiler.add_c_linker_flag(sys.argv[j])
                j += 1

        elif arg == "-lcpp":
            j = i + 1
            while (j < argvNumber) and not ('-' in sys.argv[j]):
                codeCompiler.add_cpp_linker_flag(sys.argv[j])
                j += 1

        elif arg == "-D":
            j = i + 1
            while (j < argvNumber) and not ('-' in sys.argv[j]):
                codeCompiler.add_c_macro(sys.argv[j])
                codeCompiler.add_cpp_macro(sys.argv[j])
                j += 1

        elif arg == "-Dc":
            j = i + 1
            while (j < argvNumber) and not ('-' in sys.argv[j]):
                codeCompiler.add_c_macro(sys.argv[j])
                j += 1

        elif arg == "-Dcpp":
            j = i + 1
            while (j < argvNumber) and not ('-' in sys.argv[j]):
                codeCompiler.add_cpp_macro(sys.argv[j])
                j += 1

        elif arg == "-ccomp":
            codeCompiler.cCompiler = sys.argv[i + 1]

        elif arg == "-cppcomp":
            codeCompiler.cppCompiler = sys.argv[i + 1]

    # Run backend
    if isToCompile:
        codeCompiler.compile(outputFileName=outputFileName, projectDir=projectDir)
    else:
        codeCompiler.generate_makefile(projectDir=projectDir, outputFileName=outputFileName)