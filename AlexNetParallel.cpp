#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <float.h>
#include <omp.h>
#include <string>

#define NUMBER_OF_THREADS					12

#define BATCH_SIZE							125
#define CHUNK_BATCHES						20
#define NUMBER_OF_CLASSES					1000

#define ENABLE_QUANT_COEFF					1
#define ENABLE_QUANT_FEATURE_MAP_OUT		1
#define ENABLE_QUANT_ACCUMULATOR			0

#define COEFF_NUM_BITS						8
#define FM_OUT_INT							5
#define FM_OUT_FRAC							3
#define FM_ACCUM_INT						6
#define FM_ACCUM_FRAC						9
#define FM_NUM_BITS							(FM_OUT_INT + FM_OUT_FRAC)

//#define FMPRINT							1
//#define SCREENPRINT						1

void alexnet(void);
int classifier(float *sm, int size);

int main() {
    alexnet();
    //system("PAUSE");
}

/**

*/

void loadCoeff(float* coeff, int size, char *file) {
     FILE *pFile;

     pFile = fopen(file, "rb");
     if (pFile == NULL) perror("Error opening file");
     else
     {
	 fread(coeff, 4, size, pFile);
	 fclose(pFile);
     }
}

/**

*/

int compare(float *values, int size, char *file, float epsilon) {
     int i, count = 0;

     float* vars;
     vars = (float*)malloc(sizeof(float)*size);

     FILE *pFile;

     pFile = fopen(file, "rb");
     if (pFile == NULL) perror("Error opening file");
     else
     {
	fread(vars, 4, size, pFile);
	fclose(pFile);
     }

	
     for (i = 0; i < size; i++) {
	if (fabs(values[i] - vars[i]) > epsilon) {
	    if (count < 1) {
		printf("%d\n", i);
		printf("%2.9f\n", vars[i]);
		printf("%2.9f\n", values[i]);
	    }
	    count++;
	}
     }
     free(vars);
     return count;
}


/**

*/


inline void conv_layer_1(int x, int y, float* feature_map, int channels, int depth, float *coeff, int filter_size, int stride, int padding, float* out_fm, int offset, float* bias /*float dz_dy, float dz_dw*/) {

     int i, j, l, m, n, k;

     int square = filter_size*filter_size;
     int size = x*y;
     int cs = channels*square;
     //output size
     int x_out = (x - filter_size + 2 * padding) / stride + 1;
     int y_out = (y - filter_size + 2 * padding) / stride + 1;
     int x_y_out = x_out*y_out;
     //(227-11+2*0)/4 + 1

     //#pragma omp parallel for
     for (k = 0; k < depth; k++) {

	 int off = (k + offset)*cs;
	 int depth_x_y = k*x_out*y_out;
	 for (i = 0; i < x_out; i++) {
	      int ssi = i*stride - padding;
	      for (j = 0; j < y_out; j++) {
		   int ssj = j*stride - padding;
		   int pixel = i*y_out + j;
		   out_fm[depth_x_y + pixel] = 0;

		   for (l = 0; l < channels; l++) {

			int l_x_y = l*size;

			for (m = 0; m < filter_size; m++) {
			     for (n = 0; n < filter_size; n++) {

				if (((ssi /*- (filter_size-1)/2*/ + m) < x) && ((ssj /*-(filter_size-1)/2*/ + n) < y)) {
				    if (((ssi /*- (filter_size-1)/2*/ + m) >= 0) && ((ssj /*-(filter_size-1)/2*/ + n) >= 0)) {
									//if (filter_size != 3) 
					float tmp = feature_map[l_x_y + (ssi /*- (filter_size-1)/2*/ + m)*y + ssj /*-(filter_size-1)/2*/ + n] * coeff[off + l*square + m*filter_size + n] + out_fm[depth_x_y + pixel];


							out_fm[depth_x_y + pixel] = tmp;

									
								}
							}

						}
					}
				}
				

				float tmp = bias[k] + out_fm[depth_x_y + i*y_out + j];
				out_fm[depth_x_y + i*y_out + j] = tmp;
			}
		}
	}

	//backprop
}


void conv_layer_1_slow(int x, int y, float* feature_map, int channels, int depth, float *coeff, int filter_size, int stride, int padding, float* out_fm, int offset, float* bias /*float dz_dy, float dz_dw*/) {

	//float* feature_map_new = malloc(sizeof(float)*(x+2*padding)*(y+2*padding)*channels);
	int i, j, l, m, n, k;

	//output size
	int x_out = (x - filter_size + 2 * padding) / stride + 1;
	int y_out = (y - filter_size + 2 * padding) / stride + 1;
	//(227-11+2*0)/4 + 1


	//#pragma omp parallel for
	for (k = 0; k < depth; k++) {
	     for (i = 0; i < x_out; i++) {
		  for (j = 0; j < y_out; j++) {
		       for (l = 0; l < channels; l++) {
			    //convolution
			    for (m = 0; m < filter_size; m++) {
				 for (n = 0; n < filter_size; n++) {
							
				      if (((i*stride + m - padding) < x) && ((j*stride + n - padding) < y)) {
					  if (((i*stride + m - padding) >= 0) && ((j*stride + n - padding) >= 0)) {

					      out_fm[k*x_out*y_out + i*y_out + j] = out_fm[k*x_out*y_out + i*y_out + j] + coeff[(k + offset)*channels*filter_size*filter_size + l*filter_size*filter_size + m*filter_size + n] * feature_map[l*x*y + (i*stride + m - padding)*y + j*stride + n - padding];
									
					  }
				      }

				 }
			    }
			}
			out_fm[k*x_out*y_out + i*y_out + j] = out_fm[k*x_out*y_out + i*y_out + j] + bias[k];

		  }
	      }
	 }

	//backprop
}

inline void pooling_layer(int x, int y, float* feature_map, int depth, int size, int stride, int padding, float *out_fm /*float dz_dy, float dz_dy*/) {

    int x_out = (x - size) / stride + 1;
    int y_out = (y - size) / stride + 1;

    int k, i, j, m, n;

    for (k = 0; k < depth; k++) {
	int kxy = k*x*y;
	int k_xo_yo = k*x_out*y_out;
	for (i = 0; i < x_out; i++) {
	    int ssi = i*stride;
	    for (j = 0; j < y_out; j++) {
		//convolution
		int ssj = j*stride;
		float max = 0.0;
		for (m = 0; m < size; m++) {
		    for (n = 0; n < size; n++) {
			if (feature_map[kxy + y*(ssi + m) + ssj + n] > max)
			    max = feature_map[kxy + y*(ssi + m) + ssj + n];

		    }
		}
		out_fm[k_xo_yo + i*y_out + j] = max;
	    }
	}

    }

    //backprop    
}

/**

*/

void pooling_layer_slow(int x, int y, float* feature_map, int depth, int size, int stride, int padding, float *out_fm /*float dz_dy, float dz_dy*/) {

     int x_out = (x - size) / stride + 1;
     int y_out = (y - size) / stride + 1;

     //printf("pooling size %d\n", x_out);

     int k, i, j, m, n;

     for (k = 0; k < depth; k++) {
	 for (i = 0; i < x_out; i++) {
	      for (j = 0; j < y_out; j++) {
		   //convolution
		   float max = 0.0;
	 	   for (m = 0; m < size; m++) {
			for (n = 0; n < size; n++) {
			    if (feature_map[k*x*y + y*(i*stride + m) + j*stride + n] > max)
				max = feature_map[k*x*y + y*(i*stride + m) + j*stride + n];

			}
		   }
		   out_fm[k*x_out*y_out + i*y_out + j] = max;
	      }
	 }

     }
}

/**

*/

void relu_layer(int x, int y, float* feature_map, int depth, int size, int stride, float *out_fm /*float dz_dy, float dz_dw*/) {

    int k, i, j;

    for (k = 0; k < depth; k++) {
	for (i = 0; i < x; i++) {
	     for (j = 0; j < y; j++) {

		if (feature_map[k*x*y + i*y + j] > 0) {
		    out_fm[k*x*y + i*y + j] = feature_map[k*x*y + i*y + j];
		}

		else {
		    out_fm[k*x*y + i*y + j] = 0;
		}
	     }
	}
    }
}

/**

*/

void nonlinear(int x, int y, float* feature_map, int depth, int size, int stride, int type /*float dz_dy, float dz_dy_in*/) {

    //sigmoid
    int k, i, j;

    for (k = 0; k < depth; k++) {
	for (i = 0; i < x; i++) {
	     for (j = 0; j < y; j++) {
		 feature_map[k*x*y + i*y + j] = 1 / (1 + exp(-feature_map[k*x*y + i*y + j]));
				
	     }
	}
    }
}


/**

*/

void memzero(float *fm, int size) {
    int i;
    for (i = 0; i < size; i++) {
	fm[i] = 0;
    }
}

void softmax(float* feature_map, int size, float* out_fm /*float dz_dy, float dz_dy_in*/) {
	
    float sm[NUMBER_OF_CLASSES];
    //memzero(sm, NUMBER_OF_CLASSES);
    float sum = 0.0;
    int i;

    float m = maxv(feature_map, size);

    for (i = 0; i < size; i++) {
	sm[i] = exp(feature_map[i] - m);
	sum = sum + sm[i];
    }

    for (i = 0; i < size; i++) {
	out_fm[i] = sm[i] / sum;
    }

    //free(sm);
}

int classifier(float *sm, int size) {
    int i, idx = 0;
    float max = sm[0];
    for (i = 1; i < size; i++) {
	if (sm[i] > max) {
	    max = sm[i];
	    idx = i;
	}
    }
    return idx;
}



float *swap(float *sm, int idx_1, int idx_2){

    float temp;
    temp = sm[idx_1];
    sm[idx_1] = sm[idx_2];
    sm[idx_2] = temp;
    return sm;
}

int *top5(float *sm, int size, int k) {


    int i, j;
    int max_idx;
    float max_val;
    int top5_idx[5];
	
    top5_idx = (int *)malloc(sizeof(int)*5);
    for (i = 0; i < k; i++) {
        max_idx = i;
        max_val = sm[i];

        for (j = i + 1; j < size; j++){
            if (sm[j] > max_val) {
	        max_idx = j;
	        max_val = sm[j];
            }
        }
		
        sm = swap(sm, i, max_idx);
        top5_idx[i] = max_idx + 1;
		
     }
	

     return top5_idx;
}

/**

*/

void alexnet(void) {

	int i, j;
	//	struct timeval timeStart, timeEnd;
	float *coeff_1, *coeff_2, *coeff_3, *coeff_4, *coeff_5, *coeff_6, *coeff_7, *coeff_8;
	float *bias_1, *bias_2, *bias_3, *bias_4, *bias_5, *bias_6, *bias_7, *bias_8;
	int count_images = 0;
	int count_matlab_match = 0;
	int count_c_code_match = 0;
	int count_matlab_match_top5 = 0;
	int count_c_code_match_top5 = 0;

	coeff_1 = (float *)malloc(11 * 11 * 3 * 96 * sizeof(float));
	coeff_2 = (float *)malloc(5 * 5 * 48 * 256 * sizeof(float));
	coeff_3 = (float *)malloc(3 * 3 * 256 * 384 * sizeof(float));
	coeff_4 = (float *)malloc(3 * 3 * 192 * 384 * sizeof(float));
	coeff_5 = (float *)malloc(3 * 3 * 192 * 256 * sizeof(float));
	coeff_6 = (float *)malloc(6 * 6 * 256 * 4096 * sizeof(float));
	coeff_7 = (float *)malloc(1 * 1 * 4096 * 4096 * sizeof(float));
	coeff_8 = (float *)malloc(1 * 1 * 4096 * 1000 * sizeof(float));

	bias_1 = (float *)malloc(96 * sizeof(float));
	bias_2 = (float *)malloc(256 * sizeof(float));
	bias_3 = (float *)malloc(384 * sizeof(float));
	bias_4 = (float *)malloc(384 * sizeof(float));
	bias_5 = (float *)malloc(256 * sizeof(float));
	bias_6 = (float *)malloc(4096 * sizeof(float));
	bias_7 = (float *)malloc(4096 * sizeof(float));
	bias_8 = (float *)malloc(1000 * sizeof(float));


	loadCoeff(coeff_1, 11 * 11 * 3 * 96, "conv1.bin");
	loadCoeff(coeff_2, 5 * 5 * 48 * 256, "conv2.bin");
	loadCoeff(coeff_3, 3 * 3 * 256 * 384, "conv3.bin");
	loadCoeff(coeff_4, 3 * 3 * 192 * 384, "conv4.bin");
	loadCoeff(coeff_5, 3 * 3 * 192 * 256, "conv5.bin");
	loadCoeff(coeff_6, 6 * 6 * 256 * 4096, "conv6.bin");
	loadCoeff(coeff_7, 1 * 1 * 4096 * 4096, "conv7.bin");
	loadCoeff(coeff_8, 1 * 1 * 4096 * 1000, "conv8.bin");

	
	loadCoeff(bias_1, 96, "bias1.bin");
	loadCoeff(bias_2, 256, "bias2.bin");
	loadCoeff(bias_3, 384, "bias3.bin");
	loadCoeff(bias_4, 384, "bias4.bin");
	loadCoeff(bias_5, 256, "bias5.bin");
	loadCoeff(bias_6, 4096, "bias6.bin");
	loadCoeff(bias_7, 4096, "bias7.bin");
	loadCoeff(bias_8, 1000, "bias8.bin");

	

	float* feature_map = (float*)malloc(sizeof(float) * 227 * 227 * 3 * BATCH_SIZE);
	float* feature_map_1 = (float*)malloc(sizeof(float) * 55 * 55 * 96 * NUMBER_OF_THREADS);
	float* feature_map_11 = (float*)malloc(sizeof(float) * 55 * 55 * 96 * NUMBER_OF_THREADS);
	float* feature_map_12 = (float*)malloc(sizeof(float) * 27 * 27 * 96 * NUMBER_OF_THREADS);
	float* feature_map_2 = (float*)malloc(sizeof(float) * 27 * 27 * 256 * NUMBER_OF_THREADS);
	float* feature_map_21 = (float*)malloc(sizeof(float) * 27 * 27 * 256 * NUMBER_OF_THREADS);
	float* feature_map_22 = (float*)malloc(sizeof(float) * 27 * 27 * 256 * NUMBER_OF_THREADS);
	float* feature_map_3 = (float*)malloc(sizeof(float) * 13 * 13 * 384 * NUMBER_OF_THREADS);
	float* feature_map_31 = (float*)malloc(sizeof(float) * 13 * 13 * 384 * NUMBER_OF_THREADS);
	float* feature_map_4 = (float*)malloc(sizeof(float) * 13 * 13 * 384 * NUMBER_OF_THREADS);
	float* feature_map_41 = (float*)malloc(sizeof(float) * 13 * 13 * 384 * NUMBER_OF_THREADS);
	float* feature_map_5 = (float*)malloc(sizeof(float) * 13 * 13 * 256 * NUMBER_OF_THREADS);
	float* feature_map_51 = (float*)malloc(sizeof(float) * 13 * 13 * 256 * NUMBER_OF_THREADS);
	float* feature_map_52 = (float*)malloc(sizeof(float) * 6 * 6 * 256 * NUMBER_OF_THREADS);
	float* feature_map_6 = (float*)malloc(sizeof(float) * 1 * 1 * 4096 * NUMBER_OF_THREADS);
	float* feature_map_61 = (float*)malloc(sizeof(float) * 1 * 1 * 4096 * NUMBER_OF_THREADS);
	float* feature_map_7 = (float*)malloc(sizeof(float) * 1 * 1 * 4096 * NUMBER_OF_THREADS);
	float* feature_map_71 = (float*)malloc(sizeof(float) * 1 * 1 * 4096 * NUMBER_OF_THREADS);
	float* feature_map_8 = (float*)malloc(sizeof(float) * 1 * 1 * 1000 * NUMBER_OF_THREADS);
	float* smax = (float*)malloc(sizeof(float) * 1 * 1 * 1000 * NUMBER_OF_THREADS);

	memzero(feature_map, 227 * 227 * 3 * BATCH_SIZE);
	memzero(feature_map_1, 55 * 55 * 96 * NUMBER_OF_THREADS);
	memzero(feature_map_11, 55 * 55 * 96 * NUMBER_OF_THREADS);
	memzero(feature_map_12, 27 * 27 * 96 * NUMBER_OF_THREADS);
	memzero(feature_map_2, 27 * 27 * 256 * NUMBER_OF_THREADS);
	memzero(feature_map_21, 27 * 27 * 256 * NUMBER_OF_THREADS);
	memzero(feature_map_22, 27 * 27 * 256 * NUMBER_OF_THREADS);
	memzero(feature_map_3, 13 * 13 * 384 * NUMBER_OF_THREADS);
	memzero(feature_map_31, 13 * 13 * 384 * NUMBER_OF_THREADS);
	memzero(feature_map_4, 13 * 13 * 384 * NUMBER_OF_THREADS);
	memzero(feature_map_41, 13 * 13 * 384 * NUMBER_OF_THREADS);
	memzero(feature_map_5, 13 * 13 * 256 * NUMBER_OF_THREADS);
	memzero(feature_map_51, 13 * 13 * 256 * NUMBER_OF_THREADS);
	memzero(feature_map_52, 6 * 6 * 256 * NUMBER_OF_THREADS);
	memzero(feature_map_6, 1 * 1 * 4096 * NUMBER_OF_THREADS);
	memzero(feature_map_61, 1 * 1 * 4096 * NUMBER_OF_THREADS);
	memzero(feature_map_7, 1 * 1 * 4096 * NUMBER_OF_THREADS);
	memzero(feature_map_71, 1 * 1 * 4096 * NUMBER_OF_THREADS);
	memzero(feature_map_8, 1 * 1 * 1000 * NUMBER_OF_THREADS);
	memzero(smax, 1 * 1 * 1000 * NUMBER_OF_THREADS);

	//////////////////////////////////////////////////////////////////////////////////
	FILE *pFile, *pFile_1st, *pFile_2nd, *pFile_3rd, *pFile_4th, *pFile_5th, *flog;
	FILE *pLabels;

	float result_1st[BATCH_SIZE*CHUNK_BATCHES];
	float result_2nd[BATCH_SIZE*CHUNK_BATCHES];
	float result_3rd[BATCH_SIZE*CHUNK_BATCHES];
	float result_4th[BATCH_SIZE*CHUNK_BATCHES];
	float result_5th[BATCH_SIZE*CHUNK_BATCHES];

	float labels[BATCH_SIZE*CHUNK_BATCHES];
	int chunk;
	int cl;
	cl = 3;
	int *cl_top5;
	
	char file_str[256];
	char  file_str_1st[256];
	char  file_str_2nd[256];
	char  file_str_3rd[256];
	char  file_str_4th[256];
	char  file_str_5th[256];

	char  label_str[256];

	cl_top5 = (int *)malloc(sizeof(int) * 5);

	flog = fopen("matching_results.txt", "w");

	FILE *statistics_file = fopen("Chunks_statistics.txt", "w");
	for (chunk = 0; chunk < 20; chunk++)
	{

		
		printf("(AlexNet) Testing chunk #%d with floating-point coefficients vs. floating-point data\n\n", chunk+1);
		snprintf(file_str_1st, sizeof(file_str_1st), "predicted_1st_float_MatConvNet_%d", chunk + 1);
		snprintf(file_str_2nd, sizeof(file_str_2nd), "predicted_2nd_float_MatConvNet_%d", chunk + 1);
		snprintf(file_str_3rd, sizeof(file_str_3rd), "predicted_3rd_float_MatConvNet_%d", chunk + 1);
		snprintf(file_str_4th, sizeof(file_str_4th), "predicted_4th_float_MatConvNet_%d", chunk + 1);
		snprintf(file_str_5th, sizeof(file_str_5th), "predicted_5th_float_MatConvNet_%d", chunk + 1);
		

		snprintf(label_str, sizeof(label_str), "labels3_%d.bin", chunk + 1);
		pFile_1st = fopen(file_str_1st, "rb");
		pFile_2nd = fopen(file_str_2nd, "rb");
		pFile_3rd = fopen(file_str_3rd, "rb");
		pFile_4th = fopen(file_str_4th, "rb");
		pFile_5th = fopen(file_str_5th, "rb");
		pLabels = fopen(label_str, "rb");



		if (pFile_1st == NULL) perror("Error opening file");
		else
		{
			fread(result_1st, 4, BATCH_SIZE*CHUNK_BATCHES, pFile_1st);
		}
		fclose(pFile_1st);
		//------------------------------------------
		if (pFile_2nd == NULL) perror("Error opening file");
		else
		{
			fread(result_2nd, 4, BATCH_SIZE*CHUNK_BATCHES, pFile_2nd);
		}
		fclose(pFile_2nd);
		//---------------------------------------------------
		if (pFile_3rd == NULL) perror("Error opening file");
		else
		{
			fread(result_3rd, 4, BATCH_SIZE*CHUNK_BATCHES, pFile_3rd);
		}
		fclose(pFile_3rd);
		//---------------------------------------------------------
		if (pFile_4th == NULL) perror("Error opening file");
		else
		{
			fread(result_4th, 4, BATCH_SIZE*CHUNK_BATCHES, pFile_4th);
		}
		fclose(pFile_4th);
		//-----------------------------------------------------------
		if (pFile_5th == NULL) perror("Error opening file");
		else
		{
			fread(result_5th, 4, BATCH_SIZE*CHUNK_BATCHES, pFile_5th);
		}
		fclose(pFile_5th);




		if (pLabels == NULL) perror("Error opening file");
		else
		{
			fread(labels, 4, BATCH_SIZE*CHUNK_BATCHES, pLabels);
		}
		fclose(pLabels);


		snprintf(file_str, sizeof(file_str), "imdb3_%d.bin", chunk + 1);
		pFile = fopen(file_str, "rb");

		if (pFile == NULL) perror("Error opening file");
		else
		{

			int counter_par[NUMBER_OF_THREADS];
			int correct_matconvnet[NUMBER_OF_THREADS];
			int correct_matconvnet_top5[NUMBER_OF_THREADS];
			int correct_label[NUMBER_OF_THREADS];
			int correct_label_top5[NUMBER_OF_THREADS];
			memset(counter_par, 0, NUMBER_OF_THREADS);
			memset(correct_label, 0, NUMBER_OF_THREADS);
			memset(correct_label_top5, 0, NUMBER_OF_THREADS);
			memset(correct_matconvnet, 0, NUMBER_OF_THREADS);
			memset(correct_matconvnet_top5, 0, NUMBER_OF_THREADS);

			int counter = 0;
			int counter2 = 0;
			int batch_number = 0;
			
			int predicted_by_c_code[CHUNK_BATCHES*BATCH_SIZE];
			int predicted_by_c_code_top5[CHUNK_BATCHES*BATCH_SIZE][5];


			omp_set_num_threads(NUMBER_OF_THREADS);

			float* inputs_im = (float*)malloc(227 * 227 * 3 * BATCH_SIZE*sizeof(float));

			for (j = 0; j < CHUNK_BATCHES; j++) {
				fread(inputs_im, 4, 227 * 227 * 3 * BATCH_SIZE, pFile);

#pragma omp parallel for schedule(static,1) shared(feature_map_1, feature_map_11, feature_map_12, feature_map_2, feature_map_21, feature_map_22, feature_map_3, feature_map_31, feature_map_4, feature_map_41, feature_map_5, feature_map_51, feature_map_52, feature_map_6, feature_map_61)
				for (i = 0; i < BATCH_SIZE; i++) {

					int tid = omp_get_thread_num();

#ifdef FMPRINT		
					FILE *f0;
					f0 = fopen("feature_map", "wb");
					fwrite(feature_map, sizeof(*feature_map), 227 * 227 * 3, f0);
					fclose(f0);

#endif

					conv_layer_1(227, 227, inputs_im + i * 227 * 227 * 3, 3, 96, coeff_1, 11, 4, 0, feature_map_1 + tid * 55 * 55 * 96, 0, bias_1);
#ifdef FMPRINT				
					FILE *f1;
					f1 = fopen("feature_map_1", "wb");
					fwrite(feature_map_1, sizeof(*feature_map_1), 55 * 55 * 96, f1);
					fclose(f1);
#endif

					relu_layer(55, 55, feature_map_1 + tid * 55 * 55 * 96, 96, 55 * 55 * 96, 0, feature_map_11 + tid * 55 * 55 * 96);
					

					pooling_layer(55, 55, feature_map_11 + tid * 55 * 55 * 96, 96, 3, 2, 0, feature_map_12 + tid * 27 * 27 * 96);

					conv_layer_1(27, 27, feature_map_12 + tid * 27 * 27 * 96, 48, 128, coeff_2, 5, 1, 2, feature_map_2 + tid * 27 * 27 * 256, 0, bias_2);
					conv_layer_1(27, 27, feature_map_12 + 27 * 27 * 48 + tid * 27 * 27 * 96, 48, 128, coeff_2, 5, 1, 2, feature_map_2 + 27 * 27 * 128 + tid * 27 * 27 * 256, 128, bias_2 + 128);
#ifdef FMPRINT
					FILE *f2;
					f2 = fopen("feature_map_2", "wb");
					fwrite(feature_map_2, sizeof(*feature_map_2), 27 * 27 * 256, f2);
					fclose(f2);

#endif				
					relu_layer(27, 27, feature_map_2 + tid * 27 * 27 * 256, 256, 27 * 27 * 256, 0, feature_map_21 + tid * 27 * 27 * 256);
					

					pooling_layer(27, 27, feature_map_21 + tid * 27 * 27 * 256, 256, 3, 2, 0, feature_map_22 + tid * 27 * 27 * 256);
					conv_layer_1(13, 13, feature_map_22 + tid * 27 * 27 * 256, 256, 384, coeff_3, 3, 1, 1, feature_map_3 + tid * 13 * 13 * 384, 0, bias_3);
#ifdef FMPRINT
					FILE *f3;
					f3 = fopen("feature_map_3", "wb");
					fwrite(feature_map_3, sizeof(*feature_map_3), 13 * 13 * 384, f3);
					fclose(f3);
#endif
					relu_layer(13, 13, feature_map_3 + tid * 13 * 13 * 384, 384, 13 * 13 * 384, 0, feature_map_31 + tid * 13 * 13 * 384);
					

					conv_layer_1(13, 13, feature_map_31 + tid * 13 * 13 * 384, 192, 192, coeff_4, 3, 1, 1, feature_map_4 + tid * 13 * 13 * 384, 0, bias_4);
					conv_layer_1(13, 13, feature_map_31 + 13 * 13 * 192 + tid * 13 * 13 * 384, 192, 192, coeff_4, 3, 1, 1, feature_map_4 + 13 * 13 * 192 + tid * 13 * 13 * 384, 192, bias_4 + 192);
#ifdef FMPRINT
					FILE *f4;
					f4 = fopen("feature_map_4", "wb");
					fwrite(feature_map_4, sizeof(*feature_map_4), 13 * 13 * 384, f4);
					fclose(f4);
#endif
					relu_layer(13, 13, feature_map_4 + tid * 13 * 13 * 384, 384, 13 * 13 * 384, 0, feature_map_41 + tid * 13 * 13 * 384);
					

					conv_layer_1(13, 13, feature_map_41 + tid * 13 * 13 * 384, 192, 128, coeff_5, 3, 1, 1, feature_map_5 + tid * 13 * 13 * 256, 0, bias_5);
					conv_layer_1(13, 13, feature_map_41 + 13 * 13 * 192 + tid * 13 * 13 * 384, 192, 128, coeff_5, 3, 1, 1, feature_map_5 + 13 * 13 * 128 + tid * 13 * 13 * 256, 128, bias_5 + 128);
#ifdef FMPRINT
					FILE *f5;
					f5 = fopen("feature_map_5", "wb");
					fwrite(feature_map_5, sizeof(*feature_map_5), 13 * 13 * 256, f5);
					fclose(f5);
#endif
					relu_layer(13, 13, feature_map_5 + tid * 13 * 13 * 256, 256, 13 * 13 * 256, 0, feature_map_51 + tid * 13 * 13 * 256);
					

					pooling_layer(13, 13, feature_map_51 + tid * 13 * 13 * 256, 256, 3, 2, 0, feature_map_52 + tid * 6 * 6 * 256);

					conv_layer_1(6, 6, feature_map_52 + tid * 6 * 6 * 256, 256, 4096, coeff_6, 6, 1, 0, feature_map_6 + tid * 4096, 0, bias_6);
#ifdef FMPRINT
					FILE *f6;
					f6 = fopen("feature_map_6", "wb");
					fwrite(feature_map_6, sizeof(*feature_map_6), 1 * 1 * 4096, f6);
					fclose(f6);
#endif
					relu_layer(1, 1, feature_map_6 + tid * 4096, 4096, 1 * 1 * 4096, 0, feature_map_61 + tid * 4096);
					

					conv_layer_1(1, 1, feature_map_61 + tid * 4096, 4096, 4096, coeff_7, 1, 1, 0, feature_map_7 + tid * 4096, 0, bias_7);

#ifdef FMPRINT
					FILE *f7; 
					f7 = fopen("feature_map_7", "wb");
					fwrite(feature_map_7, sizeof(*feature_map_7), 1 * 1 * 4096, f7);
					fclose(f7);

#endif
					relu_layer(1, 1, feature_map_7 + tid * 4096, 4096, 1 * 1 * 4096, 0, feature_map_71 + tid * 4096);
					

					conv_layer_1(1, 1, feature_map_71 + tid * 4096, 4096, 1000, coeff_8, 1, 1, 0, feature_map_8 + tid * 1000, 0, bias_8);
#ifdef FMPRINT
					FILE *f8;
					f8 = fopen("feature_map_8", "wb");
					fwrite(feature_map_8, sizeof(*feature_map_8), 1 * 1 * 1000, f8);
					fclose(f8);
#endif

					softmax(feature_map_8 + tid * 1000, 1 * 1 * 1000, smax + tid * 1000);

					cl = 1 + classifier(smax + tid * 1000, 1000);
					cl_top5 = top5(smax + tid * 1000, 1000, 5);
					predicted_by_c_code[j*BATCH_SIZE + i] = cl;
					

					for (int l = 0; l < 5; l++)
					{
					    predicted_by_c_code_top5[j*BATCH_SIZE + i][l] = cl_top5[l];
					}
					if ((float)cl == labels[j*BATCH_SIZE + i]) {
#ifdef SCREENPRINT
						printf("Chunk:%d\t Image:%d\t  Ground truth:%d\t  MatConvNet:%d\t C Code:%d\n", chunk + 1, j*BATCH_SIZE + i + 1, (int)labels[j*BATCH_SIZE + i], (int)result[j*BATCH_SIZE + i], (int)cl);
#endif SCREENPRINT
						correct_label[tid] = correct_label[tid] + 1;

					}
				
					else {
#ifdef SCREENPRINT
						printf("Chunk:%d\t Image:%d\t  Ground truth:%d\t  MatConvNet:%d\t C Code:%d\n", chunk + 1, j*BATCH_SIZE + i + 1, (int)labels[j*BATCH_SIZE + i], (int)result[j*BATCH_SIZE + i], (int)cl);
						printf("MISMATCH! Chunk:%d\t Image:%d\t  Ground truth:%d\t  MatConvNet:%d\t C Code:%d\n", chunk + 1, j*BATCH_SIZE + i + 1, (int)labels[j*BATCH_SIZE + i], (int)result[j*BATCH_SIZE + i], (int)cl);
#endif SCREENPRINT
					}

					if ((float)cl_top5[0] == labels[j*BATCH_SIZE + i] || (float)cl_top5[1] == labels[j*BATCH_SIZE + i] || (float)cl_top5[2] == labels[j*BATCH_SIZE + i] || (float)cl_top5[3] == labels[j*BATCH_SIZE + i] || (float)cl_top5[4] == labels[j*BATCH_SIZE + i]){
						correct_label_top5[tid] = correct_label_top5[tid] + 1;
					}

					if (result_1st[j*BATCH_SIZE + i] == labels[j*BATCH_SIZE + i] || result_2nd[j*BATCH_SIZE + i] == labels[j*BATCH_SIZE + i] || result_3rd[j*BATCH_SIZE + i] == labels[j*BATCH_SIZE + i] || result_4th[j*BATCH_SIZE + i] == labels[j*BATCH_SIZE + i] || result_5th[j*BATCH_SIZE + i] == labels[j*BATCH_SIZE + i])
					{
						correct_matconvnet_top5[tid] = correct_matconvnet_top5[tid] + 1;
					}

					if ((float)cl == result_1st[j*BATCH_SIZE + i]) {
						correct_matconvnet[tid] = correct_matconvnet[tid] + 1;
						
					}

					else  {
						//printf("");
					}

					memzero(feature_map_12 + tid * 27 * 27 * 96, 27 * 27 * 96);
					memzero(feature_map_22 + tid * 27 * 27 * 256, 27 * 27 * 256);
					memzero(feature_map_52 + tid * 6 * 6 * 256, 6 * 6 * 256);

				}

				for (int k = 0; k < BATCH_SIZE; k++) {
					if (predicted_by_c_code[j*BATCH_SIZE + k] == labels[j*BATCH_SIZE + k]) {
						count_c_code_match++;
					}
					
					if (predicted_by_c_code_top5[j*BATCH_SIZE + k][0] == labels[j*BATCH_SIZE + k] || predicted_by_c_code_top5[j*BATCH_SIZE + k][1] == labels[j*BATCH_SIZE + k] || predicted_by_c_code_top5[j*BATCH_SIZE + k][2] == labels[j*BATCH_SIZE + k] || predicted_by_c_code_top5[j*BATCH_SIZE + k][3] == labels[j*BATCH_SIZE + k] || predicted_by_c_code_top5[j*BATCH_SIZE + k][4] == labels[j*BATCH_SIZE + k]) {
						count_c_code_match_top5++;
					}

					if (labels[j*BATCH_SIZE + k] == result_1st[j*BATCH_SIZE + k]) {
						count_matlab_match++;
					}
					if (labels[j*BATCH_SIZE + k] == result_1st[j*BATCH_SIZE + k] || labels[j*BATCH_SIZE + k] == result_2nd[j*BATCH_SIZE + k] || labels[j*BATCH_SIZE + k] == result_3rd[j*BATCH_SIZE + k] || labels[j*BATCH_SIZE + k] == result_4th[j*BATCH_SIZE + k] || labels[j*BATCH_SIZE + k] == result_5th[j*BATCH_SIZE + k]) {
						count_matlab_match_top5++;
					}


					fprintf(statistics_file, "Chunk:%d\t Image:%d\t  Ground truth:%d\t  MatConvNet:%d\t C Code:%d\n", chunk + 1, j*BATCH_SIZE + k + 1, (int)labels[j*BATCH_SIZE + k], (int)result_1st[j*BATCH_SIZE + k], predicted_by_c_code[j*BATCH_SIZE + k]);
					count_images++;
				}
				fprintf(statistics_file, "(AlexNet) Images processed: %d | Top_1 error(M): %2.2f%% | Top_5 error(M): %2.2f%%  | \t Top_1 error(C): %2.2f%% |Top_5 error(C): %2.2f%% \n", count_images, 100 * (1 - ((float)count_matlab_match) / count_images), 100 * (1 - ((float)count_matlab_match_top5) / count_images), 100 * (1 - ((float)count_c_code_match) / count_images), 100 * (1 - ((float)count_c_code_match_top5) / count_images));
				printf("(AlexNet) Images processed: %d | Top_1 error(M): %2.2f%% | Top_5 error(M): %2.2f%%  | \t Top_1 error(C): %2.2f%% |Top_5 error(C): %2.2f%% \n", count_images, 100 * (1 - ((float)count_matlab_match) / count_images), 100 * (1 - ((float)count_matlab_match_top5) / count_images), 100 * (1 - ((float)count_c_code_match) / count_images), 100 * (1 - ((float)count_c_code_match_top5) / count_images));

			}
			fclose(pFile);

		}
	}
	//fprintf(statistics_file, "Top 1 error matlab %f : \t Top 1 error c code %f  \n", (float)(1 - count_matlab_match / 50000) * 100, (float)(1 - count_c_code_match / 50000) * 100);
	fclose(statistics_file);


	free(coeff_1);
	free(coeff_2);
	free(coeff_3);
	free(coeff_4);
	free(coeff_5);
	free(coeff_6);
	free(coeff_7);
	free(coeff_8);

	free(feature_map);
	free(feature_map_1);
	free(feature_map_11);
	free(feature_map_12);
	free(feature_map_2);
	free(feature_map_21);
	free(feature_map_22);
	free(feature_map_3);
	free(feature_map_31);
	free(feature_map_4);
	free(feature_map_41);
	free(feature_map_5);
	free(feature_map_51);
	free(feature_map_52);
	free(feature_map_6);
	free(feature_map_61);
	free(feature_map_7);
	free(feature_map_71);
	free(feature_map_8);
}
