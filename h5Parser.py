import h5py


class Weight:
    def __init__(self, name, subname, shape, matrix):
        self.name = name
        self.subname = subname
        self.shape = shape
        self.matrix = matrix

    #def printSth(self):
    #    print(self.name, self.subname, self.shape)


def readH5(filePath):
    file = h5py.File(filePath, 'r')
    #listOfWeights = []
    keys = list(file.keys())
    fileNumber = 0
    for name in keys:
        keys2 = list(file[name].keys())
        for subname in keys2:
            fileNumber = fileNumber + 1

            #fileName = "bias"+str(fileNumber)+".bin"
            #print(fileName)

            #Save bias list to file - biasXX.bin
            biasFile = open("bias"+str(fileNumber)+".bin", "wb")
            data = file[name][subname]["bias"][0]
            biasFile.write(data.tobytes())
            biasFile.close()

            #Print fileName and shapeOfMatrix
            #print("coeff" + str(fileNumber) + ".bin")
            #print(shape, "\n")

            #fileName = "coeff" + str(fileNumber) + ".bin"
            #print(fileName)

            #Save coeffs matrix to file - coeffXX.bin
            coeffFile = open("coeff" + str(fileNumber) + ".bin", "wb")
            for i in range(len(file[name][subname]["kernel"])):
                data = file[name][subname]["kernel"][i]
                coeffFile.write(data.tobytes())
            coeffFile.close()
    #return listOfWeights


filePath = 'vgg16.h5'
weights = readH5(filePath)