"""
To run this script the following packages are required:
 + Keras
 + Theano
 + h5py

Based on https://keras.io/applications/
"""

# Set Theano as backend for Keras
import os
os.environ['KERAS_BACKEND'] = 'theano'  # can be tensorflow or cntk

from keras.applications.resnet50 import ResNet50
from common import saveModelToFiles

if __name__ == '__main__':

    # Pre-trained model to test
    modelResNet50 = ResNet50()

    # Save model
    saveModelToFiles(modelResNet50, 'resnet50')

    # TODO: generate C code
    # TODO: run Python neural network
    # TODO: run C neural network
    # TODO: compare result