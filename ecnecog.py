"""
Command-line code generator for convolutional neural networks
@date 2018-12-01
"""

import sys
import AlexnetParser
import VggParser
import CodeGen
import CodeCompiler

# Lists of pointers' definitions
feature_maps_pointers_def = []
biases_pointers_def = []
coeff_pointers_def = []

# Lists of pointers to malloc
feature_maps_pointers_alloc = []
biases_pointers_alloc = []
coeff_pointers_alloc = []

# List of loading coeffs code
biases_pointers_load = []
coeff_pointers_load = []

# Lists of pointers to free
feature_maps_free = []
biases_free = []
coeff_free = []

# List with code to do every layer
layers_calls = []

# Which pointer (value) points to which file (key) with parameters
bias_files = {}
coeff_files = {}

biases_counter = 1
coeffs_counter = 1
feature_maps_counter = 1
input_size = ""

def print_help():
    """
    Print help on standard output
    """
    print("ecnecog.py - Excellent Convolutional NEtworks COde Generator v. 0.1")
    print("Usage: python ecnecog.py [path_to_json_file] -[l|p] [-c]")
    print("     -h, --help              print this help and exit")
    print("     -c compile after generation with default settings")
    print("Example: python ecnecog.py alexnet.json -p -c")
    sys.exit(0)

def _find_next_layers(layer, layers):
    to_return = {}
    for l in layers:
        if (not l.isInputLayer) and layer.name == l.input[0]:
            to_return[l] = _find_next_layers(l, layers)
    return to_return

def _make_pointers(flow_dict, upper_layer=None):
    # Create biases, coeffs and feature maps pointers
    # Generate layers calls

    global biases_counter
    global coeffs_counter
    global feature_maps_counter
    
    ftr_ptr_name = "feature_map_" + str(feature_maps_counter)
    for l in flow_dict.keys():
        if l.className == "Convolution2D" or l.className == "Conv2D":

            # Pointers for bias
            ptr_name = ""
            try:
                ptr_name = bias_files[l.config.biasFile]
            except:
                ptr_name = "bias_" + str(biases_counter)
                bias_files[l.config.biasFile] = ptr_name
                biases_counter += 1
                size_in_str = str(l.config.depth)
                biases_pointers_def.append("float* " + ptr_name + ";")
                biases_pointers_alloc.append(ptr_name + " = (float *)malloc(" + size_in_str + " * sizeof(float));")
                biases_pointers_load.append("loadCoeff(" + ptr_name + ", " + size_in_str + ", \"" + l.config.biasFile + "\");")
                biases_free.append("free(" + ptr_name + ");")

            l.biasPointerName = ptr_name

            # Pointers for coeffs
            ptr_name = ""
            try:
                ptr_name = coeff_files[l.config.coeffFile]
            except:
                ptr_name = "coeff_" + str(coeffs_counter)
                coeff_files[l.config.coeffFile] = ptr_name
                coeffs_counter += 1
                size_in_str = str(l.config.filterSize * l.config.filterSize * l.config.channels * l.config.depth)
                coeff_pointers_def.append("float* " + ptr_name + ";")
                coeff_pointers_alloc.append(ptr_name + " = (float *)malloc(" + size_in_str + " * sizeof(float));")
                coeff_pointers_load.append("loadCoeff(" + ptr_name + ", " + size_in_str + ", \"" + l.config.coeffFile + "\");")
                coeff_free.append("free(" + ptr_name + ");")

            l.coeffPointerName = ptr_name

            size_in_str = str(l.config.x * l.config.y * l.config.channels)
            if not ("float* " + ftr_ptr_name + ";" in feature_maps_pointers_def):
                feature_maps_pointers_def.append("float* " + ftr_ptr_name + ";")
                feature_maps_pointers_alloc.append(ftr_ptr_name + " = (float *)malloc(" + size_in_str + " * NUMBER_OF_THREADS *sizeof(float));")
                feature_maps_pointers_alloc.append("memzero(" + ftr_ptr_name + ", " + size_in_str + " * NUMBER_OF_THREADS);")
                feature_maps_free.append("free(" + ftr_ptr_name + ");")
                feature_maps_counter += 1

            l.inputDataPointerName = ftr_ptr_name
            l.inputSizeInStr = size_in_str
        elif l.className == "Relu" or l.className == "relu":
            size_in_str = str(l.config.x * l.config.y * l.config.depth)
            if not ("float* " + ftr_ptr_name + ";" in feature_maps_pointers_def):
                feature_maps_pointers_def.append("float* " + ftr_ptr_name + ";")
                feature_maps_pointers_alloc.append(ftr_ptr_name + " = (float *)malloc(" + size_in_str + " * NUMBER_OF_THREADS *sizeof(float));")
                feature_maps_pointers_alloc.append("memzero(" + ftr_ptr_name + ", " + size_in_str + " * NUMBER_OF_THREADS);")
                feature_maps_free.append("free(" + ftr_ptr_name + ");")
                feature_maps_counter += 1

            l.inputDataPointerName = ftr_ptr_name
            l.inputSizeInStr = size_in_str
        elif l.className == "Pooling" or l.className == "MaxPooling2D":
            size_in_str = str(l.config.x * l.config.y * l.config.depth)
            if not ("float* " + ftr_ptr_name + ";" in feature_maps_pointers_def):
                feature_maps_pointers_def.append("float* " + ftr_ptr_name + ";")
                feature_maps_pointers_alloc.append(ftr_ptr_name + " = (float *)malloc(" + size_in_str + " * NUMBER_OF_THREADS *sizeof(float));")
                feature_maps_pointers_alloc.append("memzero(" + ftr_ptr_name + ", " + size_in_str + " * NUMBER_OF_THREADS);")
                feature_maps_free.append("free(" + ftr_ptr_name + ");")
                feature_maps_counter += 1

            l.inputDataPointerName = ftr_ptr_name
            l.inputSizeInStr = size_in_str
        elif l.className == "Softmax" or l.className == "softmax":
            size_in_str = str(l.config.size)
            if not ("float* " + ftr_ptr_name + ";" in feature_maps_pointers_def):
                feature_maps_pointers_def.append("float* " + ftr_ptr_name + ";")
                feature_maps_pointers_alloc.append(ftr_ptr_name + " = (float *)malloc(" + size_in_str + " * NUMBER_OF_THREADS *sizeof(float));")
                feature_maps_pointers_alloc.append("memzero(" + ftr_ptr_name + ", " + size_in_str + " * NUMBER_OF_THREADS);")
                feature_maps_free.append("free(" + ftr_ptr_name + ");")
                feature_maps_counter += 1

            l.inputDataPointerName = ftr_ptr_name
            l.inputSizeInStr = size_in_str
        elif l.className == "Flatten":
            size_in_str = str(l.config.x * l.config.y * l.config.depth)
            if not ("float* " + ftr_ptr_name + ";" in feature_maps_pointers_def):
                feature_maps_pointers_def.append("float* " + ftr_ptr_name + ";")
                feature_maps_pointers_alloc.append(ftr_ptr_name + " = (float *)malloc(" + size_in_str + " * NUMBER_OF_THREADS *sizeof(float));")
                feature_maps_pointers_alloc.append("memzero(" + ftr_ptr_name + ", " + size_in_str + " * NUMBER_OF_THREADS);")
                feature_maps_free.append("free(" + ftr_ptr_name + ");")
                feature_maps_counter += 1
            l.inputDataPointerName = ftr_ptr_name
            l.inputSizeInStr = size_in_str
        elif l.className == "Dense":
            size_in_str = str(l.config.x * l.config.y * l.config.depth)
            if not ("float* " + ftr_ptr_name + ";" in feature_maps_pointers_def):
                feature_maps_pointers_def.append("float* " + ftr_ptr_name + ";")
                feature_maps_pointers_alloc.append(ftr_ptr_name + " = (float *)malloc(" + size_in_str + " * NUMBER_OF_THREADS *sizeof(float));")
                feature_maps_pointers_alloc.append("memzero(" + ftr_ptr_name + ", " + size_in_str + " * NUMBER_OF_THREADS);")
                feature_maps_free.append("free(" + ftr_ptr_name + ");")
                feature_maps_counter += 1
            l.inputDataPointerName = ftr_ptr_name
            l.inputSizeInStr = size_in_str
        else:
            print("PointersMaker: Unknown class of layer: " + l.className)
            
        _make_pointers(flow_dict[l], l)

def _make_code(layers_flow_dict):
    global input_size

    for l in layers_flow_dict.keys():
        if l.className == "Convolution2D" or l.className == "Conv2D":
            mul = "tid"
            if l.isInputLayer:
                mul = "i"
                input_size = l.inputSizeInStr
            output_offset = ""
            if l.config.outputOffset != 0:
                output_offset = "+(" + str(l.config.outputOffset) + ")"
            input_offset = ""
            if l.config.inputOffset != 0:
                input_offset = "+(" + str(l.config.inputOffset) + ")"
            biasOffset = ""
            if l.config.offset != 0:
                biasOffset = "+(" + str(l.config.offset) + ")"
            call = "conv_layer_1(" + \
                    str(l.config.x) + ", " + \
                    str(l.config.y) + ", " + \
                    l.inputDataPointerName + input_offset + "+" + mul + "*" + l.inputSizeInStr + ", " + \
                    str(l.config.channels) + ", " + \
                    str(l.config.depth) + ", " + \
                    l.coeffPointerName + ", " + \
                    str(l.config.filterSize) + ", " + \
                    str(l.config.stride) + ", " + \
                    str(l.config.padding) + ", " + \
                    l.outputDataPointerName + output_offset + " + tid * " + l.outputSizeInStr + ", " + \
                    str(l.config.offset) + ", " + \
                    l.biasPointerName + biasOffset + ");"

            layers_calls.append(call)
            #if l.isOutputLayer:
                #if config["name"] == "alexnet":
                #    layers_calls.append("int cl = 1 + classifier(" + l.outputDataPointerName + output_offset + " + tid * " + l.outputSizeInStr + ");")
                #    layers_calls.append("int cl_top5 = top5(" + l.outputDataPointerName + output_offset + " + tid * " + l.outputSizeInStr + ", " + l.outputSizeInStr + ", 5);")
        elif l.className == "Relu" or l.className == "relu":
            mul = "tid"
            if l.isInputLayer:
                mul = "i"
                input_size = l.inputSizeInStr
            output_offset = ""
            if l.config.outputOffset != 0:
                output_offset = "+(" + str(l.config.outputOffset) + ")"
            input_offset = ""
            if l.config.inputOffset != 0:
                input_offset = "+(" + str(l.config.inputOffset) + ")"
            call = "relu_layer(" + \
                    str(l.config.x) + ", " + \
                    str(l.config.y) + ", " + \
                    l.inputDataPointerName + input_offset + "+" + mul + "*" + l.inputSizeInStr + ", " + \
                    str(l.config.depth) + ", " + \
                    str(l.config.size) + ", " + \
                    str(l.config.stride) + ", " + \
                    l.outputDataPointerName + output_offset + " + tid * " + l.outputSizeInStr + ");"

            layers_calls.append(call)
            #if l.isOutputLayer:
                #if config["name"] == "alexnet":
                #    layers_calls.append("int cl = 1 + classifier(" + l.outputDataPointerName + output_offset + " + tid * " + l.outputSizeInStr + ");")
                #    layers_calls.append("int cl_top5 = top5(" + l.outputDataPointerName + output_offset + " + tid * " + l.outputSizeInStr + ", " + l.outputSizeInStr + ", 5);")

        elif l.className == "Pooling" or l.className == "MaxPooling2D":
            mul = "tid"
            if l.isInputLayer:
                mul = "i"
                input_size = l.inputSizeInStr
            output_offset = ""
            if l.config.outputOffset != 0:
                output_offset = "+(" + str(l.config.outputOffset) + ")"
            input_offset = ""
            if l.config.inputOffset != 0:
                input_offset = "+(" + str(l.config.inputOffset) + ")"
            call = "pooling_layer(" + \
                    str(l.config.x) + ", " + \
                    str(l.config.y) + ", " + \
                    l.inputDataPointerName + input_offset + "+" + mul + "*" + l.inputSizeInStr + ", " + \
                    str(l.config.depth) + ", " + \
                    str(l.config.size) + ", " + \
                    str(l.config.stride) + ", " + \
                    str(l.config.padding) + ", " + \
                    l.outputDataPointerName + output_offset + " + tid * " + l.outputSizeInStr + ");"

            layers_calls.append(call)
            #if l.isOutputLayer:
            #    if config["name"] == "alexnet":
            #        layers_calls.append("int cl = 1 + classifier(" + l.outputDataPointerName + output_offset + " + tid * " + l.outputSizeInStr + ");")
            #        layers_calls.append("int cl_top5 = top5(" + l.outputDataPointerName + output_offset + " + tid * " + l.outputSizeInStr + ", " + l.outputSizeInStr + ", 5);")

        elif l.className == "Softmax" or l.className == "sotfmax":
            mul = "tid"
            if l.isInputLayer:
                mul = "i"
                input_size = l.inputSizeInStr
            output_offset = ""
            if l.config.outputOffset != 0:
                output_offset = "+(" + str(l.config.outputOffset) + ")"
            input_offset = ""
            if l.config.inputOffset != 0:
                input_offset = "+(" + str(l.config.inputOffset) + ")"
            call = "softmax(" + \
                    l.inputDataPointerName + input_offset + "+" + mul + "*" + l.inputSizeInStr + ", " + \
                    str(l.config.size) + ", " + \
                    l.outputDataPointerName + output_offset + " + tid * " + l.outputSizeInStr + ");"
            
            layers_calls.append(call)

            #if l.isOutputLayer:
                #if config["name"] == "alexnet":
                #    layers_calls.append("int cl = 1 + classifier(" + l.outputDataPointerName + output_offset + " + tid * " + l.outputSizeInStr + ", " + l.outputSizeInStr + ");")
                #    layers_calls.append("int cl_top5 = top5(" + l.outputDataPointerName + output_offset + " + tid * " + l.outputSizeInStr + ", " + l.outputSizeInStr + ", 5);")
        elif l.className == "Flatten":
            mul = "tid"
            if l.isInputLayer:
                mul = "i"
                input_size = l.inputSizeInStr
            output_offset = ""
            if l.config.outputOffset != 0:
                output_offset = "+(" + str(l.config.outputOffset) + ")"
            input_offset = ""
            if l.config.inputOffset != 0:
                input_offset = "+(" + str(l.config.inputOffset) + ")"
            call = "flatten(" + \
                    l.inputDataPointerName + input_offset + "+" + mul + "*" + l.inputSizeInStr + ", " + \
                    l.outputDataPointerName + output_offset + " + tid * " + l.outputSizeInStr + ");"
            
            layers_calls.append(call)
        elif l.className == "Dense":
            mul = "tid"
            if l.isInputLayer:
                mul = "i"
                input_size = l.inputSizeInStr
            output_offset = ""
            if l.config.outputOffset != 0:
                output_offset = "+(" + str(l.config.outputOffset) + ")"
            input_offset = ""
            if l.config.inputOffset != 0:
                input_offset = "+(" + str(l.config.inputOffset) + ")"
            call = "dense(" + \
                    l.inputDataPointerName + input_offset + "+" + mul + "*" + l.inputSizeInStr + ", " + \
                    l.outputDataPointerName + output_offset + " + tid * " + l.outputSizeInStr + ");"
            
            layers_calls.append(call)
        else:
            print("CodeMaker: Unknown class of layer: " + l.className)
            

    for l in layers_flow_dict.keys():
        _make_code(layers_flow_dict[l])

if __name__ == "__main__":

    argvNumber = len(sys.argv)

    # Not enough arguments
    if argvNumber == 1:
        print_help()

    # Parse input parameters
    input_json_file = sys.argv[1]
    compile_after_generation = False
    is_project = False
    is_lib = True
    for arg in sys.argv[2:]:
        if arg == "-l":
            is_lib = True
        if arg == "-p":
            is_project = True
        if arg == "-c":
            compile_after_generation = True
    
    # Parse input JSON
    layers = VggParser.readJson(input_json_file)
    net_name = "vgg16"
    number_of_threads = 1
    batch_size = 1
    chunk_batches = 1

    # All leveles of layers
    layers_flow = {}

    # Look for first layers
    for l in layers:
        if l.isInputLayer:
            layers_flow[l] = _find_next_layers(l, layers)

    # Make basic call from layers' flow
    _make_pointers(layers_flow)

    # Find output feature maps for each level
    for l in layers:

        # Look for the last layer
        if l.isOutputLayer:
            # Add final output feature map
            if not ("float* feature_map_out;" in feature_maps_pointers_def):
                feature_maps_pointers_def.append("float* feature_map_out;")
                feature_maps_pointers_alloc.append("feature_map_out = (float *)malloc(" + l.inputSizeInStr + " * NUMBER_OF_THREADS *sizeof(float));")
                feature_maps_pointers_alloc.append("memzero(feature_map_out, " + l.inputSizeInStr + " * NUMBER_OF_THREADS);")
                feature_maps_free.append("free(feature_map_out);")

            l.outputDataPointerName = "feature_map_out"
            l.outputSizeInStr = l.inputSizeInStr
    for i in layers:
        for l in layers:
            if i.name in l.input:
                i.outputDataPointerName = l.inputDataPointerName
                i.outputSizeInStr = l.inputSizeInStr

    # Create layers once again
    layers_flow = {}
    for l in layers:
        if l.isInputLayer:
            layers_flow[l] = _find_next_layers(l, layers)

    # Now the final code can be created
    _make_code(layers_flow)

    # Generate code
    c_filename = "main.c"
    c_code = CodeGen.CppFile(c_filename)
    c_code("#define NUMBER_OF_CLASSES " + str(1000))
    c_code("#include \"layers.h\"")
    c_code("#include <stdio.h>")
    c_code("#include <stdlib.h>")
    c_code("#include <string.h>")
    c_code("#include <math.h>")
    c_code("#include <float.h>")
    c_code("#include <omp.h>")
    c_code("#define NUMBER_OF_THREADS " + str(number_of_threads))
    c_code("#define BATCH_SIZE " + str(batch_size))
    c_code("#define CHUNK_BATCHES " + str(chunk_batches))

    with c_code.block("void " + net_name + "(void)"):
        for c in coeff_pointers_def:
            c_code(c)
        for b in biases_pointers_def:
            c_code(b)
        for f in feature_maps_pointers_def:
            c_code(f)
        for f in feature_maps_pointers_alloc:
            c_code(f)
        for c in coeff_pointers_alloc:
            c_code(c)
        for b in biases_pointers_alloc:
            c_code(b)
        for c in coeff_pointers_load:
            c_code(c)
        for b in biases_pointers_load:
            c_code(b)

        c_code("FILE *pFile;")

        input_files = "{"
        for i in ["vgg16_input.bin"]:
            input_files += "\"" + i + "\","
        input_files = input_files[:-1] + "};"
        c_code("const char* inputFiles[] = " + input_files)
        c_code("omp_set_num_threads(NUMBER_OF_THREADS);")
        with c_code.block("for (int inputFileIndex = 0; inputFileIndex < CHUNK_BATCHES; inputFileIndex++)"):
            c_code("int tid = omp_get_thread_num();")
            c_code("pFile = fopen(inputFiles[inputFileIndex], \"rb\");")
            c_code("if (pFile == NULL) { perror(\"Cannot open input file\"); break; }")

            c_code("fread(feature_map_1, 4, " + input_size + " * BATCH_SIZE, pFile);")
            c_code("fclose(pFile);")
            
            features_map_pointers_name = [i.split(' ')[1][:-1] for i in feature_maps_pointers_def]
            c_code("#pragma omp parallel for schedule(static,1) shared(" + ", ".join(features_map_pointers_name) + ")")
            with c_code.block("for (int i = 0; i < BATCH_SIZE; i++)"):
                for call_code in layers_calls:
                    c_code(call_code)

        for b in biases_free:
            c_code(b)
        for c in coeff_free:
            c_code(c)
        for f in feature_maps_free:
            c_code(f)

    with c_code.block("int main()"):
        c_code(net_name + "();")
        c_code("return 0;")

    c_code.close()

    if compile_after_generation:
        c_compiler = CodeCompiler.CodeCompiler()
        c_compiler.add_source_to_compile(c_filename)
        c_compiler.add_include_dir('.')
        c_compiler.add_c_flag("-fopenmp")
        c_compiler.add_c_flag("-lm")
        c_compiler.compile()