CC=gcc
CXX=g++
CFLAGS=-fopenmp -lm --std=c11 -O0 -I.
CXXFLAGS=--std=c++17 -O0 -I.
C_LDFLAGS=
CXX_LDFLAGS=
C_SRCS=main.c
C_OBJS=$(C_SRCS:.c=.o)
CXX_OBJS= 
STATIC_LIBS= 
BUILD_DIR=_build
BUILD_DIRS=$(BUILD_DIR) 
OUTPUT=output
%.o: %.c
	$(CC) -c $< -o $(BUILD_DIR)/$@ $(CFLAGS) $(C_LDFLAGS)
%.o: %.cpp
	$(CXX) -c $< -o $(BUILD_DIR)/$@ $(CXXFLAGS) $(CXX_LDFLAGS)
%.o: %.cc
	$(CXX) -c $< -o $(BUILD_DIR)/$@ $(CXXFLAGS) $(CXX_LDFLAGS)
main:
	$(CC) $(addprefix $(BUILD_DIR)/,$(C_OBJS)) $(STATIC_LIBS) -o $(OUTPUT) $(CFLAGS) $(C_LDFLAGS)
createBuildDirs:
	-mkdir $(BUILD_DIRS)
all: createBuildDirs $(C_OBJS) $(CXX_OBJS) main
clean:
	-rm -r $(BUILD_DIR)
.PHONY: clean