#ifndef _LAYERS_H
#define _LAYERS_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <float.h>

void loadCoeff(float* coeff, int size, char *file) {
     FILE *pFile;

     pFile = fopen(file, "rb");
     if (pFile == NULL) perror("Error opening file");
     else
     {
	 fread(coeff, 4, size, pFile);
	 fclose(pFile);
     }
}

/**

*/

int compare(float *values, int size, char *file, float epsilon) {
     int i, count = 0;

     float* vars;
     vars = (float*)malloc(sizeof(float)*size);

     FILE *pFile;

     pFile = fopen(file, "rb");
     if (pFile == NULL) perror("Error opening file");
     else
     {
	fread(vars, 4, size, pFile);
	fclose(pFile);
     }

	
     for (i = 0; i < size; i++) {
	if (fabs(values[i] - vars[i]) > epsilon) {
	    if (count < 1) {
		printf("%d\n", i);
		printf("%2.9f\n", vars[i]);
		printf("%2.9f\n", values[i]);
	    }
	    count++;
	}
     }
     free(vars);
     return count;
}


/**

*/


void conv_layer_1(int x, int y, float* feature_map, int channels, int depth, float *coeff, int filter_size, int stride, int padding, float* out_fm, int offset, float* bias /*float dz_dy, float dz_dw*/) {

     int i, j, l, m, n, k;

     int square = filter_size*filter_size;
     int size = x*y;
     int cs = channels*square;
     //output size
     int x_out = (x - filter_size + 2 * padding) / stride + 1;
     int y_out = (y - filter_size + 2 * padding) / stride + 1;
     int x_y_out = x_out*y_out;
     //(227-11+2*0)/4 + 1

     //#pragma omp parallel for
     for (k = 0; k < depth; k++) {

	 int off = (k + offset)*cs;
	 int depth_x_y = k*x_out*y_out;
	 for (i = 0; i < x_out; i++) {
	      int ssi = i*stride - padding;
	      for (j = 0; j < y_out; j++) {
		   int ssj = j*stride - padding;
		   int pixel = i*y_out + j;
		   out_fm[depth_x_y + pixel] = 0;

		   for (l = 0; l < channels; l++) {

			int l_x_y = l*size;

			for (m = 0; m < filter_size; m++) {
			     for (n = 0; n < filter_size; n++) {

				if (((ssi /*- (filter_size-1)/2*/ + m) < x) && ((ssj /*-(filter_size-1)/2*/ + n) < y)) {
				    if (((ssi /*- (filter_size-1)/2*/ + m) >= 0) && ((ssj /*-(filter_size-1)/2*/ + n) >= 0)) {
									//if (filter_size != 3) 
					float tmp = feature_map[l_x_y + (ssi /*- (filter_size-1)/2*/ + m)*y + ssj /*-(filter_size-1)/2*/ + n] * coeff[off + l*square + m*filter_size + n] + out_fm[depth_x_y + pixel];


							out_fm[depth_x_y + pixel] = tmp;

									
								}
							}

						}
					}
				}
				

				float tmp = bias[k] + out_fm[depth_x_y + i*y_out + j];
				out_fm[depth_x_y + i*y_out + j] = tmp;
			}
		}
	}

	//backprop
}


void conv_layer_1_slow(int x, int y, float* feature_map, int channels, int depth, float *coeff, int filter_size, int stride, int padding, float* out_fm, int offset, float* bias /*float dz_dy, float dz_dw*/) {

	//float* feature_map_new = malloc(sizeof(float)*(x+2*padding)*(y+2*padding)*channels);
	int i, j, l, m, n, k;

	//output size
	int x_out = (x - filter_size + 2 * padding) / stride + 1;
	int y_out = (y - filter_size + 2 * padding) / stride + 1;
	//(227-11+2*0)/4 + 1


	//#pragma omp parallel for
	for (k = 0; k < depth; k++) {
	     for (i = 0; i < x_out; i++) {
		  for (j = 0; j < y_out; j++) {
		       for (l = 0; l < channels; l++) {
			    //convolution
			    for (m = 0; m < filter_size; m++) {
				 for (n = 0; n < filter_size; n++) {
							
				      if (((i*stride + m - padding) < x) && ((j*stride + n - padding) < y)) {
					  if (((i*stride + m - padding) >= 0) && ((j*stride + n - padding) >= 0)) {

					      out_fm[k*x_out*y_out + i*y_out + j] = out_fm[k*x_out*y_out + i*y_out + j] + coeff[(k + offset)*channels*filter_size*filter_size + l*filter_size*filter_size + m*filter_size + n] * feature_map[l*x*y + (i*stride + m - padding)*y + j*stride + n - padding];
									
					  }
				      }

				 }
			    }
			}
			out_fm[k*x_out*y_out + i*y_out + j] = out_fm[k*x_out*y_out + i*y_out + j] + bias[k];

		  }
	      }
	 }

	//backprop
}

void pooling_layer(int x, int y, float* feature_map, int depth, int size, int stride, int padding, float *out_fm /*float dz_dy, float dz_dy*/) {

    int x_out = (x - size) / stride + 1;
    int y_out = (y - size) / stride + 1;

    int k, i, j, m, n;

    for (k = 0; k < depth; k++) {
	int kxy = k*x*y;
	int k_xo_yo = k*x_out*y_out;
	for (i = 0; i < x_out; i++) {
	    int ssi = i*stride;
	    for (j = 0; j < y_out; j++) {
		//convolution
		int ssj = j*stride;
		float max = 0.0;
		for (m = 0; m < size; m++) {
		    for (n = 0; n < size; n++) {
			if (feature_map[kxy + y*(ssi + m) + ssj + n] > max)
			    max = feature_map[kxy + y*(ssi + m) + ssj + n];

		    }
		}
		out_fm[k_xo_yo + i*y_out + j] = max;
	    }
	}

    }

    //backprop    
}

/**

*/

void pooling_layer_slow(int x, int y, float* feature_map, int depth, int size, int stride, int padding, float *out_fm /*float dz_dy, float dz_dy*/) {

     int x_out = (x - size) / stride + 1;
     int y_out = (y - size) / stride + 1;

     //printf("pooling size %d\n", x_out);

     int k, i, j, m, n;

     for (k = 0; k < depth; k++) {
	 for (i = 0; i < x_out; i++) {
	      for (j = 0; j < y_out; j++) {
		   //convolution
		   float max = 0.0;
	 	   for (m = 0; m < size; m++) {
			for (n = 0; n < size; n++) {
			    if (feature_map[k*x*y + y*(i*stride + m) + j*stride + n] > max)
				max = feature_map[k*x*y + y*(i*stride + m) + j*stride + n];

			}
		   }
		   out_fm[k*x_out*y_out + i*y_out + j] = max;
	      }
	 }

     }
}

/**

*/

void relu_layer(int x, int y, float* feature_map, int depth, int size, int stride, float *out_fm /*float dz_dy, float dz_dw*/) {

    int k, i, j;

    for (k = 0; k < depth; k++) {
	for (i = 0; i < x; i++) {
	     for (j = 0; j < y; j++) {

		if (feature_map[k*x*y + i*y + j] > 0) {
		    out_fm[k*x*y + i*y + j] = feature_map[k*x*y + i*y + j];
		}

		else {
		    out_fm[k*x*y + i*y + j] = 0;
		}
	     }
	}
    }
}

/**

*/

void nonlinear(int x, int y, float* feature_map, int depth, int size, int stride, int type /*float dz_dy, float dz_dy_in*/) {

    //sigmoid
    int k, i, j;

    for (k = 0; k < depth; k++) {
	for (i = 0; i < x; i++) {
	     for (j = 0; j < y; j++) {
		 feature_map[k*x*y + i*y + j] = 1 / (1 + exp(-feature_map[k*x*y + i*y + j]));
				
	     }
	}
    }
}


/**

*/

void memzero(float *fm, int size) {
    int i;
    for (i = 0; i < size; i++) {
	fm[i] = 0;
    }
}

void softmax(float* feature_map, int size, float* out_fm /*float dz_dy, float dz_dy_in*/) {
	
    float sm[NUMBER_OF_CLASSES];
    //memzero(sm, NUMBER_OF_CLASSES);
    float sum = 0.0;
    int i;

    float m = maxv(feature_map, size); // maxv = max on values

    for (i = 0; i < size; i++) {
	sm[i] = exp(feature_map[i] - m);
	sum = sum + sm[i];
    }

    for (i = 0; i < size; i++) {
	out_fm[i] = sm[i] / sum;
    }

    //free(sm);
}

int classifier(float *sm, int size) {
    int i, idx = 0;
    float max = sm[0];
    for (i = 1; i < size; i++) {
	if (sm[i] > max) {
	    max = sm[i];
	    idx = i;
	}
    }
    return idx;
}



float *swap(float *sm, int idx_1, int idx_2){

    float temp;
    temp = sm[idx_1];
    sm[idx_1] = sm[idx_2];
    sm[idx_2] = temp;
    return sm;
}

int *top5(float *sm, int size, int k) { // tp5_idx as param


    int i, j;
    int max_idx;
    float max_val;
    int* top5_idx = (int *)malloc(sizeof(int)*5);
    for (i = 0; i < k; i++) {
        max_idx = i;
        max_val = sm[i];

        for (j = i + 1; j < size; j++){
            if (sm[j] > max_val) {
	        max_idx = j;
	        max_val = sm[j];
            }
        }
		
        sm = swap(sm, i, max_idx);
        top5_idx[i] = max_idx + 1;
		
     }
	

     return top5_idx;
}

#endif /* _LAYERS_H */