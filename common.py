def saveModelToFiles(model, modelName):
    """
    Save model of the given CNN to two files: JSON (with layers) and H5 (with weights).
    @param model     model to save
    @param modelName name of the model, files will be named the same
    """
    json_string = model.to_json()

    f = open(modelName + '.json', 'w')
    f.write(json_string)
    f.close()

    model.save_weights(modelName + '.h5') 